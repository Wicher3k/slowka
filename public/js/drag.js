$(function () {
    var lastMove = null;
    var dragFlag = 0;

    function handleDragStart(e) {
        //   this.style.opacity = '1';  // this / e.target is the source node.
        dragFlag = 0;
    }

    function handleTouchOver(e) {
        dragFlag +=1;
        left = window.innerWidth / 4;
        right = window.innerWidth / 2 + left;

        if (e.touches[0].clientX < left) {
            $('.backgroundGood').show();
        } else {
            $('.backgroundGood').hide();
        }
        if (e.touches[0].clientX > right) {
            $('.backgroundBad').show();
        } else {
            $('.backgroundBad').hide();
        }
        if (e.preventDefault) {
            e.preventDefault(); // Necessary. Allows us to drop.
        }

        // e.dataTransfer.dropEffect = 'move'; // See the section on the DataTransfer object.

        return false;
    }

    function handleDragOver(e) {
        dragFlag +=1;
        left = window.innerWidth / 4;
        right = window.innerWidth / 2 + left;

        if (e.clientX < left) {
            $('.backgroundGood').show();
        } else {
            $('.backgroundGood').hide();
        }
        if (e.clientX > right) {
            $('.backgroundBad').show();
        } else {
            $('.backgroundBad').hide();
        }
        if (e.preventDefault) {
            e.preventDefault(); // Necessary. Allows us to drop.
        }

        // e.dataTransfer.dropEffect = 'move'; // See the section on the DataTransfer object.

        return false;
    }

    function handleDragEnter(e) {
        // this / e.target is the current hover target.
        this.classList.add('over');
    }

    function handleDragLeave(e) {
        this.classList.remove('over'); // this / e.target is previous target element.
    }

    function handleDrop(e) {
        // this / e.target is current target element.
        left = window.innerWidth / 4;
        right = window.innerWidth / 2 + left;
        if (dragFlag>5) {
            if (e.clientX < left) {
                $('#word-good').trigger('click');
            } else if (e.clientX > right) {
                $('#word-bad').trigger('click');
            }
        }
        $('.backgroundBad,.backgroundGood').hide();
        if (e.stopPropagation) {
            e.stopPropagation(); // stops the browser from redirecting.
        }

        // See the section on the DataTransfer object.

        return false;
    }

    function handleTouchDrop(e) {
        // this / e.target is current target element.

        left = window.innerWidth / 4;
        right = window.innerWidth / 2 + left;
        if (dragFlag>5) {
            if (e.changedTouches[0].clientX < left) {
                $('#word-good').trigger('click');
            } else if (e.changedTouches[0].clientX > right) {
                $('#word-bad').trigger('click');
            }
        }
        $('.backgroundBad,.backgroundGood').hide();
        if (e.stopPropagation) {
            e.stopPropagation(); // stops the browser from redirecting.
        }

        // See the section on the DataTransfer object.

        return false;
    }

    function handleDragEnd(e) {
        // this/e.target is the source node.
        [].forEach.call(cols, function (col) {
            col.classList.remove('over');
        });
    }

    var cols = document.querySelectorAll('#drag');
    [].forEach.call(cols, function (col) {
        col.addEventListener('dragstart', handleDragStart, false);
        col.addEventListener('touchstart', handleDragStart, false);
        col.addEventListener('dragenter', handleDragEnter, false);
        col.addEventListener('dragover', handleDragOver, false);
        col.addEventListener('touchmove', handleTouchOver, false);
        col.addEventListener('dragleave', handleDragLeave, false);
        col.addEventListener('drop', handleDrop, false);
        col.addEventListener('dragend', handleDragEnd, false);
        col.addEventListener('touchend', handleTouchDrop, false);
        col.addEventListener('touchcancel', handleTouchDrop, false);
    });
});
