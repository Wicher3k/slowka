@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('general.verifyMail.verifyYourEmailAddress') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('general.verifyMail.linksended') }}
                        </div>
                    @endif

                    {{ __('general.verifyMail.linkcheck1') }}
                    {{ __('general.verifyMail.linkcheck2') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('general.verifyMail.linksend') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
