<div class="text-center p-3">
    <a class="btn btn-lg btn-outline-primary" href="{{route("categories.create")}}">@lang('buttons.createNewCategory')</a>
</div>