@extends('layouts.main')
@section('scripts')
<script>
    $(function() {
        var selectedLangs = []
        var newSelectedLangs = []
        var startLang = true;
        var languages = JSON.parse('{!! json_encode($languages) !!}');
        $('#languages').select2({
            sorter: data => data.sort((a, b) => a.text.localeCompare(b.text))
        }).on('change',function(){
           newSelectedLangs = $(this).val();
           if(newSelectedLangs.length>1){
            $('.flashcardDiv').show('slow');
            if(startLang && newSelectedLangs.length==2){
                for (var i in newSelectedLangs){
                    $('.flashcardDiv .form-group input:nth-child('+(parseInt(i)+1)+')').attr('placeholder', Lang.get('languages.'+languages[newSelectedLangs[i]].acronym))
                    $('.flashcardDiv .form-group input:nth-child('+(parseInt(i)+1)+')').attr('name', "flashcard["+languages[newSelectedLangs[i]].acronym+"][]");
                }
                startLang = false;
            }
            if(newSelectedLangs.length>$('.flashcardDiv .form-group:nth-child(1) input').length){ //dodaj jezyk
                var newLang = newSelectedLangs.filter(x => !selectedLangs.includes(x))[0];
                $('.flashcardDiv .form-group').append('<input class="form-control flashcardInput" placeholder="'+Lang.get('languages.'+languages[newLang].acronym)+'" type="text" name="flashcard['+languages[newLang].acronym+'][]" value="">');
            }
            if(newSelectedLangs.length<$('.flashcardDiv .form-group:nth-child(1) input').length){ //odejmij jezyk
                var deleteLang = selectedLangs.filter(x => !newSelectedLangs.includes(x))[0];
                $('.flashcardDiv .form-group input[name="flashcard['+languages[deleteLang].acronym+'][]"]').remove();
            }
           }else{
            $('.flashcardDiv').hide();
           }
           selectedLangs = newSelectedLangs;
        });

        $('#addFlashcard').on('click', function(){
            var txt = $('.flashcardDiv .form-group').clone().eq(0).find("input").val("");
            $('.flashcardDiv').append('<div class="form-group">'+txt.parent().html()+"</div>");
        })


    });


</script>
<style>
    .createCategory {
        min-width: 400px;
    }

    .createCategory #categoryName {
        min-width: 400px;
    }
</style>
@endsection

@section('content')

<div class="createCategory row justify-content-center m-2">
    <form id='categoryForm' action="{{ route("categories.store",["locale"=>app()->getLocale()]) }}" method='POST'>
        @csrf
        <div class="form-group">
            <label for="languages">@lang('general.notification.chooseAtLeastTwoLanguages')</label>
            <select name="languages" class="custom-select" id="languages" multiple>
                @foreach ($languages as $lang => $x)
                <option value="{{$lang}}">@lang("languages.$x->acronym")</option>
                @endforeach

            </select>
        </div>
        <div class="form-group">
            <label for="categoryName">@lang('general.categoryName')</label><br>
            <input style="width:100%;" id="categoryName" class="form-control" name="categoryName" type="text" placeholder="@lang('general.categoryName')">
        </div>
        <div class="">
            <label for="flashcards">@lang('general.flashcards')</label>
        </div>
        <div class="flashcardDiv displayNone">

            <div class="form-group">
                <input class='form-control flashcardInput' placeholder="" type="text" name="flashcard[][]" value="">
                <input class='form-control flashcardInput' placeholder="" type="text" name="flashcard[][]" value="">
            </div>
            <div class="form-group">
                <input class='form-control flashcardInput' placeholder="" type="text" name="flashcard[][]" value="">
                <input class='form-control flashcardInput' placeholder="" type="text" name="flashcard[][]" value="">
            </div>
            <div class="form-group">
                <input class='form-control flashcardInput' placeholder="" type="text" name="flashcard[][]" value="">
                <input class='form-control flashcardInput' placeholder="" type="text" name="flashcard[][]" value="">
            </div>

        </div>
        <button type="button" id="addFlashcard" class="btn btn-secondary">@lang('buttons.add') (+)</button><br><br>
        <button type="submit" class="btn btn-primary">@lang('buttons.save')</button>
    </form>

</div>
@endsection