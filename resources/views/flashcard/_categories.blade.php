<style>
    .select2-selection {
        height: 39px !important;
    }

    .select2-selection__rendered {
        margin-top: 5px;
        height: 34px !important;
    }

    .errorSelect2 {
        border: 1px solid red;
    }

    .selectheight {
        height: 39px;
        line-height: 39px;
        font-size: 28pt;
    }

    .input-group.md-form.form-sm.form-1 input {
        border: 1px solid #bdbdbd;
        border-top-right-radius: 0.25rem;
        border-bottom-right-radius: 0.25rem;
    }

    ul.card-text li:last-child {
        list-style: none
    }

    .flashcard:nth-child(-n+3) {
        display: list-item;
    }

    .flashcard:nth-child(+n+4) {
        display: none;
    }
</style>
<script>
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function(elem) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    function pulseEffect() {
        if ($('.selectedCategory').length > 0) {
            $('.categorySubmit').addClass('pulse-effect')
        } else {
            $('.categorySubmit').removeClass('pulse-effect')
        }
    }
    $(function() {
        var langFrom = '{{ $langFrom }}';
        var langTo = JSON.parse('{!! json_encode($langTo) !!}');
        var langAll = langTo.concat([langFrom]);
        var langAllNew = [];
        $('#languageFrom').val(langFrom);
        $('#languageTo').val(langTo);
        $('#languageFrom, #languageTo').select2({
            sorter: data => data.sort((a, b) => a.text.localeCompare(b.text))
        });
        $('.addCategory').on('click', function() {
            $this = $(this);
            $this.prop('disabled', true);
            var category = $this.val();
            var categoryText = $(this).siblings('.card-title').text();
            $('#selectedCategories').append(`<li><span class='selectedCategory'> ${categoryText} </span> <input class='selectedCategoryHidden' type='hidden' name='c[]' value='${category}' /> <button type="button" class="close removeCategory" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button></li>`);
            pulseEffect();

        });
        $('#selectedCategories').on('click', '.removeCategory', function() {
            var category = $(this).siblings('.selectedCategoryHidden').val();
            $('.addCategory[value="' + category + '"]').prop('disabled', false);
            $(this).parent().remove();
            pulseEffect();
        });
        $('.categoryForm').on('submit', function() {
            if ($('.selectedCategory').length == 0) {
                alert("Wybierz przynajmniej jedna kategorię");
                return false;
            }
            $('.langFromSend').val(langFrom);
            $('.langToSend').val(JSON.stringify(langTo));
        });
        $('#languageFrom').on('change', function() {
            var $languageTo = $('#languageTo');
            $languageTo.find('option').prop('disabled', false);
            $('.card-text li span.card-vocabulary-from').addClass('displayNone');
            langFrom = $(this).val();
            $languageTo.find('option[value=' + langFrom + ']').prop('disabled', true);

            var index = langTo.indexOf(langFrom);
            if (index != -1) {
                langTo.splice(index, 1);
                if (langTo.length == 0) {
                    if (langFrom == 'en') {
                        langTo = ['pl'];
                    } else {
                        langTo = ['en'];
                    }
                }
                $languageTo.val(langTo);
            }
            $languageTo.trigger('change');
            $('.card-text li span.card-vocabulary-from[data-from=' + langFrom + ']').removeClass(
                'displayNone');
            // hide when this category dont have translations
            displayAvaiableCategories();

        }).trigger('change');

        $('#languageTo').on('change', function() {
            var i = 0;
            $('.card-text li span.card-vocabulary-to').addClass('displayNone');
            $('.card-text li span.card-vocabulary-to .card-vocabulary-separator').show();
            langTo = $(this).val();
            if (langTo.length > 0 && !$('#categoriesDivError').hasClass(
                    'd-none')) { // usuniecie erroru gdy wybrano chociaz jeden jezyk do
                $('#categoriesDiv').show();
                $('#categoriesDivError').toggleClass('d-none');
                $('#languageToDiv .select2-container--below').removeClass('errorSelect2');
            }
            for (i in langTo) {
                $('.card-text li span.card-vocabulary-to[data-to=' + langTo[i] + ']').removeClass(
                    'displayNone');; // pokazanie tylko tych nas interesujacych
            }

            $('.card-text li').each(function() {
                $(this).find(
                    'span.card-vocabulary-to:not(.displayNone):last .card-vocabulary-separator'
                ).hide();
            })

            if (langTo.length == 0) { // blad gdy brak lang to
                $('#categoriesDiv').hide();
                $('#categoriesDivError').toggleClass('d-none');
                $('#languageToDiv .select2-container--below').addClass('errorSelect2');
            }
            displayAvaiableCategories();

        }).trigger('change');

        function displayAvaiableCategories() {
            let checker = (arr, target) => target.every(v => arr.includes(v));

            if (langAllNew != langAll) {
                langAllNew = langTo.concat([langFrom]);
                langAll = langAllNew
                langAll = langAll.map(function(x) {
                    return parseInt(x, 10);
                });

                $('.cardblock').show().each(function() {
                    $this = $(this);
                    let cardLang = $this.data('lang');
                    if (!checker(cardLang, langAll)) {
                        $this.hide();
                    }
                })


            }
        }
        $('.showAll').on("click", function() {
            $(this).closest('.card-text').children('.flashcard').show('slow');
            $(this).hide();
        });

        // search input
        $('#search').on('keyup', function() {
            var serach = $(this).val();
            if (serach.length == 0) {
                $('.cardblock').show();
            } else {
                $('.cardblock').hide();
                $('.allVocabularies:contains("' + serach + '")').parent().show();
            }
        });

        // download flashcards
        $(document).on("click", '#downloadModal .btn', function() {
            var typeDownload = $(this).children('.downloadType').text();
            $("#downloadBtnSubmit").val(typeDownload);
            $('.categoryFormList').submit();
        })

        $(".categoryFormList").submit(function() {
            $(this).find("li").remove();
            $(this).append($('#selectedCategories').html());
            $(this).find("li").addClass('displayNone');
        });


    });
</script>

@if ($data)


    <div class="mr-3 mt-3 ml-3">

        <form>
            <div class="form-row">
                <div id="languageFromDiv" class="form-group col-md-5">
                    {{-- <label for="languageFrom">@lang('general.from')</label> --}}
                    <select id="languageFrom" class="custom-select">
                        @foreach ($languages as $lang => $x)
                            <option value="{{ $lang }}">@lang("languages.$x->acronym")</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 d-none d-md-block selectheight text-center">
                    <i class="fas fa-angle-double-right"></i>
                </div>
                <div id="languageToDiv" class="form-group col-md-5">
                    {{-- <label for="languageTo">@lang('general.to')</label> --}}
                    <select id="languageTo" class="custom-select" multiple>
                        @foreach ($languages as $lang => $x)
                            <option value="{{ $lang }}">@lang("languages.$x->acronym")</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>

        <h1 id="categoriesDivError" class="col-12 text-center d-none">@lang('general.notification.chooseAtLeastOneLanguage')</h1>
        <main id="categoriesDiv" class="row">
            <h1 class="col-12 text-center">{{ $trans['category'] }}</h1>
            <div class="col-sm-10 row">
                @php
                    $arrKeysLang = array_keys($languages);
                @endphp
                @foreach ($data as $category)
                    {{-- <div class="col-sm-6"> --}}
                    @php
                        $flashcards = $category->flashcards->all();
                        $langAvailable = [];
                        // shuffle($flashcards);
                        if (isset($flashcards[0])) {
                            $flashcard = $flashcards[0];

                            foreach ($arrKeysLang as $lang) {
                                if (isset($flashcard->words[$lang]->value)) {
                                    $langAvailable[] = $lang;
                                }
                            }
                        }

                    @endphp
                    <div class="cardblock mt-3 col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3"
                        data-lang='{{ json_encode($langAvailable) }}'>
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <span>{{ Lang::has('categories.' . $category->name, app()->getLocale()) ? __('categories.' . $category->name) : $category->name }}</span>

                                </h5>
                                <ul class="card-text">

                                    @foreach ($flashcards as $flashcard)
                                        <li class="flashcard ">
                                            @foreach ($arrKeysLang as $langFrom)
                                                <span class='card-vocabulary card-vocabulary-from'
                                                    data-from='{{ $langFrom }}'>{{ $flashcard->words[$langFrom]->value ?? '' }}</span>
                                            @endforeach
                                            <span>-</span>
                                            @foreach ($arrKeysLang as $langTo)
                                                <span class='card-vocabulary card-vocabulary-to'
                                                    data-to='{{ $langTo }}'>{{ $flashcard->words[$langTo]->value ?? '' }}
                                                    <span class="card-vocabulary-separator">/</span></span>
                                            @endforeach
                                        </li>
                                    @endforeach
                                    <li><button
                                            class="btn btn-block-xs-only btn-outline-secondary showAll">@choice('general.categoriesAndXothers', count($category->flashcards) - 3, ['count' => count($category->flashcards) - 3])
                                        </button></li>
                                </ul>
                                <button class="btn btn-block-xs-only btn-primary addCategory"
                                    value="{{ $category->name }}">@lang('buttons.add')</button>
                                @can('update', $category)
                                    <a class="btn btn-block-xs-only btn-secondary editCategory"
                                        value="{{ $category->name }}"
                                        href="{{ route('categories.edit', ['category' => $category->id]) }}">@lang('buttons.edit')</a>
                                @endcan
                            </div>
                        </div>
                        <span class="displayNone allVocabularies">
                            @foreach ($category->flashcards as $flashcard)
                                @foreach ($arrKeysLang as $lang)
                                    {{ $flashcard->words[$lang]->value ?? 'NoneVoc' . ' ' }}
                                @endforeach
                            @endforeach
                        </span>
                    </div>
                @endforeach
            </div>
            <div class="mt-3 mb-3 mb-sm-0 order-first order-sm-2 col-sm-2">
                <div>
                    <div class="input-group md-form form-sm form-1 pl-0">
                        <div class="input-group-prepend">
                            <span class="input-group-text lighten-3" style="background-color:gray"><i
                                    class="fas fa-search text-white" aria-hidden="true"></i></span>
                        </div>
                        <input id="search" class="form-control my-0 py-1" type="text" placeholder="Search"
                            aria-label="Search">
                    </div>
                </div>
                <section class="mt-3">
                    <h2 class="text-center">@lang('general.selectedCategories')</h2>
                    <form class='categoryForm'
                        action="{{ route('categories.vocabulary', ['locale' => app()->getLocale()]) }}" method='GET'>
                        <ul id="selectedCategories"></ul>
                        <div class="text-center">
                            <button type="submit" name="start"
                                class="btn btn-lg btn-outline-primary categorySubmit">@lang('buttons.start')</button>
                        </div>
                        <input class="langFromSend" type="hidden" name="langFrom" value="pl">
                        <input class="langToSend" type="hidden" name="langTo" value="en">
                    </form>
                    <br>
                    <form class='categoryForm categoryFormList'
                        action="{{ route('categories.vocabularyList', ['locale' => app()->getLocale()]) }}"
                        method='GET'>
                        <div class="text-center">
                            <button type="button" id="downloadBtn" class="btn btn btn-outline-secondary "
                                data-toggle="modal" data-target="#downloadModal" title='@lang('buttons.start')'><i
                                    class="fas fa-download"></i></button>
                            <input type="hidden" id="downloadBtnSubmit" name="download" />
                        </div>
                        <input class="langFromSend" type="hidden" name="langFrom" value="pl">
                        <input class="langToSend" type="hidden" name="langTo" value="en">
                    </form>
                </section>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="downloadModal" tabindex="-1" role="dialog"
                aria-labelledby="downloadModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="downloadModalHeaderTitle">@lang('general.selectAListingMethod')</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">

                            {{-- <button type="button" class="btn btn-outline-primary" data-dismiss="modal"><span class="downloadType">TXT</span> <i class="fas fa-download"></i></button> --}}
                            <button type="button" class="btn btn-outline-success" data-dismiss="modal"><span
                                    class="downloadType">CSV</span> <i class="fas fa-download"></i></button>
                            <button type="button" class="btn btn-outline-success" data-dismiss="modal"><span
                                    class="downloadType">XLSX</span> <i class="fas fa-download"></i></button>
                            <br><br>
                            <button type="button" class="btn btn-outline-dark"
                                data-dismiss="modal">@lang('general.onPage')<span
                                    class="downloadType displayNone">Page</span></button>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    </div>


@endif
