@extends('profile._profileLayout')

@section('scriptsProfile')

<script>
    $(function() {

        $('body').on('click','#deactivationSubmitBtn',  function(){
            $("#deactivationForm").submit();
        });
})

</script>
@endsection

@section('contentProfile')

<div class="row">
    <div class="col-md-12">
        <h4>@lang('general.accountDactivation')</h4>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form id="deactivationForm" action="{{ route("profile.deactivationPost",["locale"=>app()->getLocale()]) }}" method='POST'>
            @csrf
            <div class="form-group row">
                <label class="col-12 col-form-label">@lang('general.deactivationText')
                </label>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <button data-toggle="modal" data-target="#deactivationModal" name="button" type="button"
                        class="btn btn-outline-danger">@lang('buttons.delete') <i class="fas fa-trash-alt"></i></button>
                </div>
            </div>

        </form>
    </div>
</div>

<div class="modal fade" id="deactivationModal" tabindex="-1" role="dialog" aria-labelledby="deactivationModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deactivationModal">@lang('general.deactivationConfirmation')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @lang('general.areYouSureToDeactivateTheAccount')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('buttons.cancel')</button>
                <button id="deactivationSubmitBtn" type="button" class="btn btn-danger">@lang('buttons.delete')</button>
            </div>
        </div>
    </div>
</div>

@endsection