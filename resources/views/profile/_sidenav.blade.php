<div class="col-md-3 ">
    <nav class="list-group">
       <a href="{{ route('profile.index',["locale"=>app()->getLocale()]) }}" class="list-group-item list-group-item-action">@lang('general.myProfile')</a>
       <a href="{{ route("myflashcards",["locale"=>app()->getLocale(), "user" => auth()->id()]) }}"
          class="list-group-item list-group-item-action">@lang('general.myflashcards')</a>
       {{-- <a href="#" class="list-group-item list-group-item-action">My Stats</a> --}}
       <a href="{{ route('profile.deactivation',["locale"=>app()->getLocale()]) }}"
          class="list-group-item list-group-item-action">@lang('general.accountDactivation')</a>
    </nav>
 </div>