@extends('layouts.main')
@section('scripts')

<script>
    $(function() {
      var url = window.location.href;
      $('nav.list-group a[href="' + url + '"]').addClass('active');
    });
</script>

@yield('scriptsProfile')

@endsection
@section('content')
<div>
    <div class="row">
        @include('profile._sidenav')
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    @yield('contentProfile')
                </div>
            </div>
        </div>
    </div>
</div>

@endsection