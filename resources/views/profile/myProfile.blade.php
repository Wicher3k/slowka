@extends('profile._profileLayout')

@section('scriptsProfile')

@endsection

@section('contentProfile')

<div class="row">
   <div class="col-md-12">
      <h4>@lang('general.myProfile')</h4>
      <hr>
   </div>
</div>

<div class="row">
   <div class="col-md-12">

      <!-- Password errors -->
      @foreach ($errors->all() as $error)
      <div class="alert alert-danger text-center">
         <div class="text-danger">{{ $error }}</div>
      </div>
      @endforeach
      {{-- @endif --}}

      <form action="{{ route("profile.update",["locale"=>app()->getLocale(), "user" => auth()->id()]) }}" method='POST'>
      @csrf
      @method("PUT")
      <div class="form-group row">
         <label for="username" class="col-4 col-xl-2 col-form-label">@lang('general.username')</label>
         <div class="col-8 col-xl-10">
            <input id="username" name="username" placeholder="@lang('general.username')" class="form-control @error('username') is-invalid @enderror"
               required="required" type="text" value='{{old('username') ?? $user->username}}'>
            @error('username')
            <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
            </span>
            @enderror
         </div>
      </div>
      <div class="form-group row">
         <label for="email" class="col-4 col-xl-2 col-form-label">E-mail</label>
         <div class="col-8 col-xl-10">
            <input id="email" name="email" placeholder="E-mail" class="form-control @error('email') is-invalid @enderror" required="required" type="text"
               value='{{old('email') ?? $user->email}}'>
            @error('email')
            <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
            </span>
            @enderror
         </div>
      </div>
      <div class="form-group row">
         <div class="offset-md-4 offset-xl-2 col-md-6  col-xl-10">
            <div class="form-check">
               <input type="checkbox" class="form-check-input " id="newsletter" name='newsletter'
                  {{ ($user->newsletter || old('newsletter')) ? 'checked' : '' }}>
               <label class="form-check-label" for="newsletter"> @lang('general.registration.newsletter') </label>
            </div>
         </div>
      </div>
      <div class="form-group row">
         <div class="offset-md-4 offset-xl-2 col-md-8 col-xl-10">
            <button name="submit" type="submit" class="btn btn-primary">@lang('buttons.updateMyProfile')</button>
            <button data-toggle="modal" data-target="#changePasswordModal" id="changePasswordBtn" name="changePassword" type="button"
               class="btn btn-secondary">@lang('buttons.changePassword')</button>
         </div>
      </div>
      </form>
   </div>
</div>


<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="changePasswordModal" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form method="post" action="{{ route("profile.changePassword",["locale"=>app()->getLocale()]) }}" id="passwordForm">
            @csrf
            <div class="modal-header">
               <h5 class="modal-title" id="changePasswordModal">@lang('general.changePassword')</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">


               <div class="form-group row">
                  <div class="col-12">
                     <input type="password" class="input-lg form-control" name="current_password" id="current_password" placeholder="@lang('general.currentPassword')"
                        autocomplete="off" required minlength="8">
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12">
                     <input type="password" class="input-lg form-control" name="new_password" id="new_password" placeholder="@lang('general.newPassword')" autocomplete="off"
                        required minlength="8">
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-12">
                     <input type="password" class="input-lg form-control" name="new_confirm_password" id="new_confirm_password"
                        placeholder="@lang('general.repeatNewPassword')" autocomplete="off" required minlength="8">
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('buttons.cancel')</button>
               <button id="deactivationSubmitBtn" type="submit" class="btn btn-primary">@lang('buttons.save')</button>
            </div>
         </form>
      </div>
   </div>
</div>

@endsection