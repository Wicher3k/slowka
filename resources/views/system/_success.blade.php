@if (session()->has('success'))
<div class="alert alert-success text-center">
    @if(is_array(session('success')))
    @foreach (session('success') as $message)
    {{ $message }}
    @endforeach
    @else
    {{ session('success') }}
    @endif
</div>
@endif