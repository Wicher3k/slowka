@extends('layouts.main')
@section('scripts')
<script>
$(function(){
    $(".hBack").on("click", function(e){
        e.preventDefault();
        window.history.back();
    });
})
</script>
@endsection
@section('content')

<div class="text-center">
    <br><button class="btn btn-secondary hBack" type="button">@lang('buttons.return2MainPage')</button> <br><br>
    @foreach ($data as $category)
    <h2>{{__('categories.'.$category->name)}}</h2>
    @foreach ($category->flashcards as $flashcard)
    <p>
        {{$flashcard->words[$from]->value}}
        -
        @foreach ($toArr as $langId)
        {{$flashcard->words[$langId]->value}}
        @endforeach

    </p>

    @endforeach
    <hr>
    @endforeach
</div>
@endsection