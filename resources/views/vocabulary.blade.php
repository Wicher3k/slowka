@extends('layouts.main')
@section('scripts')
<script src="{{ asset('js/drag.js') }}"></script>
<script>
    $(function() {

    var vocabulary = JSON.parse('{!!str_replace("'","’",json_encode($vocabulary, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES))!!}');
    var from = '{!!($from)!!}';
    var toArr = JSON.parse('{!!($toArr)!!}');
    var randomElement;
    var removedHalf = false;

    window.mobileCheck = function() {
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};
    var mobile = window.mobileCheck();

    function changeWord(){
        var id = Math.floor(Math.random() * vocabulary.length);
        randomElement = vocabulary[id];

var maxlength = randomElement[from].value.length;
maxlengthHint = 0;
        for (var i in toArr){
            if(randomElement[toArr[i]].value.length>maxlength){
                maxlength = randomElement[toArr[i]].value.length
            }
            if(  randomElement[2].word_infos != undefined){
                maxlengthHint = randomElement[2].word_infos.hint.length
            }

        }

        if(mobile && maxlength>40){
            $('.wordHeading').removeClass('wordHeadingH2').addClass('wordHeadingH3');
        }else if(mobile && maxlength>20){
             $('.wordHeading').removeClass('wordHeadingH3').addClass('wordHeadingH2');
        }else{
            $('.wordHeading').removeClass('wordHeadingH2 wordHeadingH3');
        }

        if(mobile && maxlengthHint>40){
            if(randomElement[2].word_infos != null){// zawsze z polskiego
                $('#word-from-hint').addClass('wordHeadingH4');
            }else{
                $('#word-from-hint').removeClass('wordHeadingH4');
            }

        }

        $('#word-from').html(randomElement[from].value);
        if(randomElement[2].word_infos != null){// zawsze z polskiego
            $('#word-from-hint').html(randomElement[2].word_infos.hint);
        }else{
            $('#word-from-hint').html('');
        }
        $('#word-to-div').css('visibility', 'hidden');
        for (var i in toArr){
            $('#word-to-'+i).html(randomElement[toArr[i]].value);

        }
        $('#word-id').text(id);
    }
    changeWord();
$('#word-check').on('click', function(){
    $('#word-to-div').css('visibility', '');
    if (removedHalf == false)
    {
        removeHalf();
    }
});

$('#word-bad').on('click', function(){
    activeWordId = $('#word-id').text();
    poolActualize(0);
    history(activeWordId, 0);
    decideWordOrEnd();
});

$('#word-good').on('click', function(){
    activeWordId = $('#word-id').text();
    poolActualize(1);
    history(activeWordId, 1);
    vocabulary.splice(activeWordId, 1);
    $('#start-left').text(vocabulary.length);
    decideWordOrEnd();
});

$('#footerPanel .card-footer').on("click",function(){
    $(this).parent().find('.side-body').toggle('slow');
});

$('.half').on('click', function(){
    var  half = Math.round(vocabulary.length / 2);
    for(var i= 1;i<half; i++ ){
        $('#word-good').trigger('click');
    }
});

function removeHalf(){
    removedHalf = true;
    $('.half').remove();
}

$( document ).keydown(function(e) {
    e = e || window.event;
if (e.keyCode == '38' || e.keyCode == '87' ) {
    $('#word-check').trigger('click');
}
else if (e.keyCode == '40') {
    // down arrow
}
else if (e.keyCode == '37' || e.keyCode == '65' ) {
    $('#word-good').trigger('click');
}
else if (e.keyCode == '39' || e.keyCode == '68' ) {
    $('#word-bad').trigger('click');
}
});

function decideWordOrEnd(){
    if(vocabulary.length == 0){
        $('#endModal').modal('show');
    }else{
        changeWord();
    }
}

function history(wordId, mode){
    var today  = new Date();
    var vocabularyTrans = '';
    for (var i in toArr){
        vocabularyTrans+= vocabulary[wordId][toArr[i]].value + " ";
    }
    $('.history .list').prepend("<li class='mode"+mode+"'>"+today.toLocaleTimeString() + " - " + vocabulary[wordId][from].value +  " - " + vocabularyTrans + "</li>");
}

function poolActualize(mode){
    if(mode==0){
        var $badVocabulary = $('.pool .pool-vocabular .pool-vocabular-'+randomElement[from].flashcard_id);
        $badVocabulary.each(function() {
            var $pool_vocabular_bad_number = $(this).find('.pool-vocabular-bad-number');
            var countBad = parseInt($pool_vocabular_bad_number.text()) +1.
            $pool_vocabular_bad_number.text(countBad);
            $(this).show();
        })

    }else if(mode == 1){
        $('.pool .card-good .pool-vocabular.pool-vocabular-'+randomElement[from].flashcard_id).show();
        $('.pool .card-pool .pool-vocabular.pool-vocabular-'+randomElement[from].flashcard_id).hide();
    }

}


});
</script>
<style>
    .start-reaction,
    #word-check,
    #word-to-div {
        margin-top: 30px;
    }

    #word-to-2 {
        color: darkgreen;
        margin-left: 10px;
    }

    .wordHeadingH2 {
        font-size: 2rem;
    }

    .wordHeadingH3 {
        font-size: 1.5rem;
    }

    .wordHeadingH4 {
        font-size: 1.25rem;
    }

    .btn {
        padding: 0.75rem 1rem;
    }

    h1 {
        font-size: 2.75rem;
    }

    #footerPanel .myCard .side-body {
        max-height: 40vh;
        height: 30vh;
        overflow: auto;
        resize: vertical;
    }

    #footerPanel .myCard .card-footer {
        cursor: pointer;
    }

    #footerPanel .history {
        width: 44%;
        left: 5%;
    }

    #footerPanel .pool {
        width: 44%;
        right: 5%;
    }

    #footerPanel .myCard {
        position: fixed;
        bottom: 0;
    }


    .mode0,
    .pool .pool-vocabular .pool-vocabular-bad {
        color: red
    }

    .mode1,
    .pool .card-good .pool-vocabular {
        color: green
    }

    .card-header-good {
        background-color: rgb(183, 247, 183);
    }

    .card-header-good,
    .card-header-pool {
        padding: .3rem 1.25rem;
    }
</style>
@endsection
@section('content')
<div draggable="true" id="drag" class="row h-100 justify-content-center align-items-center">
    <div class="dragDecide backgroundGood"></div>
    <div class="dragDecide backgroundBad"></div>
    <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-5">
        <div class="card border-secondary text-center">
            <div class="card-header">
                @lang('general.vacabularyLeft', ['all' => count($vocabulary)])
            </div>
            <div class="card-body">
                <h1 class="card-title wordHeading"><span id="word-from"></span></h1>
                <div id="word-to-div">
                    @foreach (json_decode($toArr) as $index => $to)
                    <h1 class="wordHeading card-title">
                        <span id="word-to-{{$index}}"></span>
                    </h1>
                    @endforeach
                    <h3>
                        <span id="word-from-hint"></span>
                    </h3>
                </div>
                <button id="word-check" class="btn col-6 btn-lg btn-primary">@lang('buttons.check')</button>

                <div class="start-reaction row">
                    <div class="col-6 ">
                        <button id='word-good' class="btn btn-block btn-lg btn-success">@lang('buttons.good')</button>
                    </div>
                    <div class="col-6 ">
                        <button id='word-bad' class="btn btn-block btn-lg btn-danger">@lang('buttons.toThePot')</button>
                    </div>
                </div>
            </div>
            <span class="d-none" id="word-id"></span>

            <div class="card-footer text-muted">
                <div class="d-none d-sm-block">
                    <span style="display:inline-block; float:left;">@lang('general.shortcuts.keyboard'):</span> <span><span
                            style='color: #007bff'>@lang('buttons.check')</span> (<b style='font-weight: bold;'>w</b>, <i class="fas fa-arrow-up"></i>)</span> |
                    <span><span style='color: #28a745'>@lang('buttons.good')</span> (<b style='font-weight: bold;'>a</b>, <i
                            class="fas fa-arrow-left"></i>)</span>
                    | <span><span style='color: #dc3545'>@lang('buttons.toThePot')</span> (<b style='font-weight: bold;'>d</b>, <i
                            class="fas fa-arrow-right"></i>)</span>

                    <span class='half' style='display: inline-block'><span  style='color: black'> </span> (<b style='font-weight: bold;'>Half</b>, <i class="fas fa-battery-half"></i>)</span>
                    </span>

                </div>
                <div class="d-block d-sm-none">
                    <span style="display:inline-block; float:left;">@lang('general.shortcuts.touch'):</span>
                    <span style='color: #28a745'>@lang('buttons.good')</span> (<i class="fas fa-reply"></i>)</span>
                    |
                    <span><span style='color: #dc3545'>@lang('buttons.toThePot')</span> (<i class="fas fa-share"></i>)</span>
                    <span class='half' style='display: inline-block'>
                    <span  style='color: black'> </span> (<b style='font-weight: bold;'>Half</b>,
                     <i class="fas fa-battery-half"></i>)</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="endModal" tabindex="-1" role="dialog" aria-labelledby="endModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <h4 class="modal-title text-center" id="exampleModalLongTitle">Gratulacje</h4> --}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title text-center" id="exampleModalLongTitle">@lang('general.goodJob')!</h2>
            </div>
            <div class="modal-footer">

                <button type="button" onclick='location.reload()' id='retry' class="btn btn-primary spin-effect mr-auto">@lang('buttons.retry') <i
                        class="fas fa-sync-alt icon-effect"></i></button>
                <form style="display: inline" action="{{ route("categories.index",["locale"=>app()->getLocale()]) }}" method="get">
                    <button type="submit" class="btn btn-violet buzz-effect">@lang('buttons.newCategories') <i class="fas icon-effect fa-store"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>

<footer class="fixed-bottom" id="footerPanel">
    <div class="container-fixed" style="background-color: lightgreen">
        <div class="text-center ">
            <div class="card myCard border-secondary text-center history">

                <div class="displayNone side-body card-body">
                    <ul class="list-unstyled list">

                    </ul>
                </div>
                <div class="card-footer">
                    @lang('general.history')
                </div>
            </div>
        </div>
        <div class="text-center ">
            <div class="card myCard border-secondary text-center pool">

                <div class="displayNone side-body card-body">
                    <div class="row no-gutters">
                        <div class="col-12 col-lg-6  ">
                            <div class="card">
                                <div class="card-header card-header-good">
                                    @lang('buttons.good')
                                </div>
                                <div class="card-body card-good">
                                    <ul>
                                        @foreach ($vocabulary as $id => $vocabular)
                                        <li class="displayNone pool-vocabular pool-vocabular-{{$vocabular[$from]['flashcard_id']}}">
                                            {{$vocabular[$from]['value']}}
                                            <span class="displayNone pool-vocabular-bad pool-vocabular-{{$vocabular[$from]['flashcard_id']}}">
                                                ( <span class="pool-vocabular-bad-number"> 0 </span> )
                                            </span>

                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-lg-6 ">
                            <div class="card">
                                <div class="card-header card-header-pool">
                                    @lang('buttons.pool')
                                </div>
                                <div class="card-body card-pool">
                                    <ul>
                                        @foreach ($vocabulary as $id => $vocabular)
                                        <li class="pool-vocabular pool-vocabular-{{$vocabular[$from]['flashcard_id']}}">
                                            {{$vocabular[$from]['value']}}
                                            <span class="displayNone pool-vocabular-bad pool-vocabular-{{$vocabular[$from]['flashcard_id']}}">
                                                ( <span class="pool-vocabular-bad-number"> 0 </span> )
                                            </span>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    @lang('general.listOfWords')
                </div>
            </div>
        </div>


    </div>
</footer>
@endsection