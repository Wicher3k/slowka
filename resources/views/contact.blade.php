@extends('layouts.main')
@section('scripts')
<style>
    .contact section p {
        margin: 20px;
    }

    .contact-icon {
        display: inline-block;
        width: 25px;
    }

    a.anchor {
        display: block;
        position: relative;
        top: -50px;
        visibility: hidden;
    }
</style>


@endsection
@section('content')
<div class="contact row h-100 align-items-center m-4">

    <a class="anchor" id="policy_privacyPolicy"></a>
    <section class="ml-3 mr-3 mb-3">
        <h1>@lang('policy.privacyPolicy.name')</h1>
        <ol>
            <li>
                @lang('policy.privacyPolicy.list.1')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.2')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.3')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.4')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.5')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.6')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.7')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.8')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.9')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.10')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.11')
            </li>
            <li>
                @lang('policy.privacyPolicy.list.12')
            </li>
        </ol>
    </section>
    <a class="anchor" id="policy_cookie"></a>
    <section class="ml-3 mr-3 mb-3">
        <h1>@lang('policy.cookie.name')</h1>
        <ol>
            <li>
                @lang('policy.cookie.list.1')
            </li>
            <li>
                @lang('policy.cookie.list.2')
            </li>
            <li>
                @lang('policy.cookie.list.3')
            </li>
            <li>
                @lang('policy.cookie.list.4')
            </li>
            <li>
                @lang('policy.cookie.list.5')
            </li>
            <li>
                @lang('policy.cookie.list.6')
            </li>
            <li>
                @lang('policy.cookie.list.7')
            </li>
            <li>
                @lang('policy.cookie.list.8')
            </li>
            <li>
                @lang('policy.cookie.list.9')
            </li>
            <li>
                @lang('policy.cookie.list.10')
            </li>
            <li>
                @lang('policy.cookie.list.11')
            </li>
        </ol>
    </section>


    <section class='ml-3 mr-3 mb-4'>
        <h1> @lang('policy.contact.name')</h1>
        <p>
            <span class="contact-icon text-center"> <i class="fas fa-user"></i> </span><span>Roland Górnisiewicz</span><br>
            <span class="contact-icon text-center"> <i class="fas fa-envelope"></i> </span><span>roland.gornisiewicz@gmail.com</span><br>
            <span class="contact-icon text-center"> <i class="fas fa-map-marker-alt"></i> </span><span>@lang('policy.contact.place')</span><br>
        </p>
    </section>

</div>

@endsection