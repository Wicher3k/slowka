<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="description" content="@lang('general.meta.description')">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Roland Górnisiewicz">
    <link rel="alternate" hreflang="en" href="https://myflashcardplace.com/en" />
    <link rel="alternate" hreflang="pl" href="https://myflashcardplace.com/pl" />
    <link rel="alternate" hreflang="x-default" href="https://myflashcardplace.com/en" />
    <title>My Flashcard Place</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="{{asset('messages.js')}}"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">


    <script>
        $(function() {
            var url = window.location.href;

    // Will only work if string in href matches with location
        $('ul.navbar-nav a[href="' + url + '"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
        // $('ull.navbar-nav a').filter(function () {
        //     return this.href == url;
        // }).parent().addClass('active').parent().parent().addClass('active');

        $('#popoverLanguage').popover({
            html : true,
            placement: 'bottom',
            sanitize: false,
            trigger: 'manual',
            title: function() {
            return $("#popover-head").html();
            },
            content: function() {
            return $("#popover-content").html();
            }
                }).click(function(e) {
                    $('.popoverAnchor').popover('hide');
            $(this).popover('toggle');
            e.stopPropagation();
        });

        $('#popoverContact').popover({
            html : true,
            placement: 'bottom',
            sanitize: false,
            trigger: 'manual',
            title: function() {
            return $("#popoverContact-head").html();
            },
            content: function() {
            return $("#popoverContact-content").html();
            }
                }).click(function(e) {
                    $('.popoverAnchor').popover('hide');
            $(this).popover('toggle');
            e.stopPropagation();
        });

    $('html').click(function(e) {
        $('#popoverLanguage').popover('hide');
        $('#popoverContact').popover('hide');
    });

    $('.localization-management-list li[data-lang={{app()->getLocale()}}]').addClass('active');

    $('#cookieNavbar-close').on('click', function(){
        $(this).closest('.cookieNavbar').remove();
        $.ajax({
            method: "POST",
            url: "/cookieSet",
            async: true,
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                cookieName: "userCookieConfirmed",
                value: true
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip();
});


    </script>

    <!-- scripts -->
    @yield('scripts')

</head>

<body>
    <div id="app">
        <div class="container-fluid h-100">
            <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                {{-- <a class="navbar-brand" href="{{ route("categories.index",["locale"=>app()->getLocale()]) }}">
                <img src="{{ asset('img/logo.png') }}" width="30" height="30" alt="logo">
                </a> --}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route("categories.index",["locale"=>app()->getLocale()]) }}">@lang('general.categories') <span
                                    class="sr-only">(@lang('general.current'))</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                                href="{{ route("myflashcards",["locale"=>app()->getLocale(), "user" => auth()->id()]) }}">@lang('general.myflashcards') <span
                                    class="sr-only">(@lang('general.current'))</span></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('profile.index',["locale"=>app()->getLocale()]) }}">{{ __('general.myProfile') }}</a>
                        </li>
                        @endauth
                        <li class="nav-item">
                            <a href="#" id="popoverLanguage" class="popoverAnchor nav-link"> <i class="fas fa-globe"></i></a>
                            <div id="popover-head" class="displayNone text-center">
                                <div class="text-center">
                                    @lang('general.language')
                                </div>
                            </div>
                            <div id="popover-content" class=" displayNone">
                                <div class="localization-management-list">
                                    <ul>
                                        <li data-lang='pl'><a href="{{ route("lang",["locale"=>"pl"]) }}"><span class="lang-name">Polski</span></a></li>
                                        <li data-lang='en'><a href="{{ route("lang",["locale"=>"en"]) }}"><span class="lang-name">English</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="#" id="popoverContact" class="popoverAnchor nav-link"> <i class="fas fa-envelope"></i></a>
                            <div id="popoverContact-head" class="displayNone">
                                <div class="text-center">
                                    @lang('policy.contact.name')
                                </div>
                            </div>
                            <div id="popoverContact-content" class="displayNone">
                                <div class="localization-management-list">
                                    <ul>
                                        <li><a href="{{ route("contact.contact",["locale"=>app()->getLocale()]) }}"><span
                                                    class="lang-name">@lang('policy.contact.name')</span></a></li>
                                        <li><a href="{{ route("contact.cookiePolicy",["locale"=>app()->getLocale()]) }}'#policy_cookie"><span class="lang-name">@lang('policy.cookie.name')</span></a></li>
                                        <li><a href="{{ route("contact.cookiePolicy",["locale"=>app()->getLocale()]) }}'#policy_privacyPolicy"><span
                                                    class="lang-name">@lang('policy.privacyPolicy.name')</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('general.login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('general.register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('general.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                        {{-- <li class="nav-item">
                        <a class="nav-link" href="{{ url('/login') }}">@lang('buttons.login')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/register') }}">@lang('buttons.register')</a>
                        </li> --}}
                    </ul>
                </div>
            </nav>
            <main style="padding-top: 60px;">
                @include('system._success')
                @yield('content')
            </main>

        </div>
        @includeWhen(Cookie::get('userCookieConfirmed') != true, 'layouts.cookieNavbar')
    </div>
</body>

</html>