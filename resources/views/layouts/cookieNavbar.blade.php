<div class="cookieNavbar text-center d-flex align-items-center justify-content-center">
    <div>
        <p> @lang('general.cookieNavbar',
            [
            'cookiePolicy' => '<a href='. route("contact.cookiePolicy",["locale"=>app()->getLocale()]).'#policy_cookie>'.__('policy.cookie.name').'</a>',
            'privacyPolicy' => '<a href='. route("contact.cookiePolicy",["locale"=>app()->getLocale()]).'#policy_privacyPolicy>'.__('policy.privacyPolicy.name').'</a>'
            ])

        </p>
    </div>
    <div>
        <button id="cookieNavbar-close" type="button" class="close" aria-label="Close">
            <span style="display:inline-block; padding: 6px 10px 6px 10px;" aria-hidden="true">&times;</span>
        </button>
    </div>
</div>