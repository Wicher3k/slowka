<?php

return [
    "colors" => "Kolory",
    "numbers" => "Liczby",
    "dateAndTime" => "Data i Godzina",

    // "food" => [
    "fruits" => "Żywność - Owoce",
    "vegetables" => "Żywność - Warzywa",
    "dairy" => "Żywność - Nabiał",
    "drinks" => "Żywność - Napoje",
    "extras" => "Żywność - Dodatki",
    "other" => "Żywność - Inne",
    // ]

    "50 Popular Verbs" => "50 Popularnych Czasowników",
    "Animals (Basics)" => "Zwierzęta (p. Podstawowy)",
    "Animals (Advanced)" => "Zwierzęta (p. Zaawansowany)",

    "Chinese - Lesson 1a - Words" => "j. chiński - Lekcja 1a - Słówka",
    "Chinese - Lesson 1a - Sentences" => "j. chiński - Lekcja 1a - Zdania",
    "Chinese - Lesson 1b - Words" => "j. chiński - Lekcja 1b - Słówka",
    "Chinese - Lesson 1b - Sentences" => "j. chiński - Lekcja 1b - Zdania",
    "Chinese - Lesson 2a - Words" => "j. chiński - Lekcja 2a - Słówka",
    "Chinese - Lesson 2a - Sentences" => "j. chiński - Lekcja 2a - Zdania",
    "Chinese - Lesson 2b - Words" => "j. chiński - Lekcja 2b - Słówka",
    "Chinese - Lesson 2b - Sentences" => "j. chiński - Lekcja 2b - Zdania",
];
