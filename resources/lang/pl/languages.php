<?php
return [

    'pl' => 'Polski',
    'en' => 'Angielski',
    'ch' => 'Chiński (pinyin)',
    'ch_2' => 'Chiński (uproszczony)',
    'ko' => 'Koreański',

];
