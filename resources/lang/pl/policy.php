<?php
return [

    "cookie" => [
        "name" => "Polityka prywatności cookie",
        "list" => [
            "1" => 'Serwis korzysta z plików cookies.',
            "2" => 'Pliki cookies (tzw. „ciasteczka”) stanowią dane informatyczne, w szczególności pliki tekstowe, które przechowywane są w urządzeniu końcowym
            Użytkownika.',
            "3" => 'Cookie usprawniają korzystanie ze stron internetowych Serwisu. Cookies zazwyczaj zawierają nazwę strony internetowej, z której
            pochodzą, czas przechowywania ich na urządzeniu końcowym oraz unikalny numer.',
            "4" => 'Podmiotem zamieszczającym na urządzeniu końcowym Użytkownika Serwisu pliki cookies oraz uzyskującym do nich dostęp jest Operator Serwisu.',
            "5" => 'Pliki cookies wykorzystywane są w celu:
            dostosowania zawartości stron internetowych Serwisu do preferencji Użytkownika oraz optymalizacji korzystania ze stron internetowych; w szczególności pliki te pozwalają rozpoznać urządzenie Użytkownika Serwisu i odpowiednio wyświetlić stronę internetową, dostosowaną do jego indywidualnych potrzeb;
            tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy Serwisu korzystają ze stron internetowych, co umożliwia ulepszanie ich struktury i zawartości;
            utrzymanie sesji Użytkownika Serwisu, dzięki której Użytkownik nie musi po powrocie do serwisu uzupełniać różnie ustawienia indywidualne użytkownika w serwisie;
            zmiany wyglądu lub wersji serwisu;
            zarządzanie pamięcia podręczną w celu optymalizacji czasu wyświetlania strony użytkownika.',
            "6" => 'W ramach Serwisu stosowane są dwa zasadnicze rodzaje plików cookies: „sesyjne” (session cookies) oraz „stałe” (persistent cookies). Cookies „sesyjne” są plikami tymczasowymi, które przechowywane są w urządzeniu końcowym Użytkownika do czasu wylogowania, opuszczenia strony internetowej lub wyłączenia oprogramowania (przeglądarki internetowej). „Stałe” pliki cookies przechowywane są w urządzeniu końcowym Użytkownika przez czas określony w parametrach plików cookies lub do czasu ich usunięcia przez Użytkownika.',
            "7" => 'W ramach Serwisu stosowane są następujące rodzaje plików cookies:<br>
            • „niezbędne” pliki cookies, umożliwiające korzystanie z usług dostępnych w ramach Serwisu, np. uwierzytelniające pliki cookies wykorzystywane do usług  wymagających uwierzytelniania w ramach Serwisu; pliki cookies służące do zapewnienia bezpieczeństwa, np. wykorzystywane do wykrywania nadużyć w zakresie uwierzytelniania w ramach Serwisu;<br>
            • „wydajnościowe” pliki cookies, umożliwiające zbieranie informacji o sposobie korzystania ze stron internetowych Serwisu;<br>
            • „funkcjonalne” pliki cookies, umożliwiające „zapamiętanie” wybranych przez Użytkownika ustawień i personalizację interfejsu Użytkownika, np. w zakresie wybranego języka lub regionu, z którego pochodzi Użytkownik, rozmiaru czcionki, wyglądu strony internetowej itp.;<br>
            • „reklamowe” pliki cookies, umożliwiające dostarczanie Użytkownikom treści reklamowych bardziej dostosowanych do ich zainteresowań.',
            "8" => 'W wielu przypadkach oprogramowanie służące do przeglądania stron internetowych (przeglądarka internetowa) domyślnie dopuszcza przechowywanie plików cookies w urządzeniu końcowym Użytkownika. Użytkownicy Serwisu mogą dokonać w każdym czasie zmiany ustawień dotyczących plików cookies. Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować automatyczną obsługę plików cookies w ustawieniach przeglądarki internetowej bądź informować o ich każdorazowym zamieszczeniu w urządzeniu Użytkownika Serwisu. Szczegółowe informacje o możliwości i sposobach obsługi plików cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej).',
            "9" => 'Operator Serwisu informuje, że ograniczenia stosowania plików cookies mogą wpłynąć na niektóre funkcjonalności dostępne na stronach internetowych Serwisu.',
            "10" => 'Pliki cookies zamieszczane w urządzeniu końcowym Użytkownika Serwisu i wykorzystywane mogą być również przez współpracujących z operatorem
            Serwisu reklamodawców oraz partnerów.',
            "11" => '
            Nazwy plików cookie, których używamy na naszej stronie internetowej, oraz cele, w których są wykorzystywane, są określone poniżej:
            używamy Google Analytics, aby rozpoznać komputer, gdy użytkownik odwiedza witrynę / śledzi użytkowników podczas przeglądania witryny / umożliwia korzystanie z koszyka na stronie / poprawić użyteczność strony internetowej / analizować korzystanie ze strony internetowej / administrować stroną internetową / zarządzać witryną / zapobiegać oszustwom i poprawić bezpieczeństwo strony internetowej / personalizować stronę internetową dla każdego użytkownika / kierować reklamy, które mogą być szczególnie interesujące dla określonych użytkowników;
            Większość przeglądarek pozwala odmówić przyjęcia plików cookie — na przykład:
            w przeglądarce Internet Explorer (wersja 10) można blokować pliki cookie, korzystając z dostępnych ustawień zastępowania obsługi plików cookie, klikając „Narzędzia”, „Opcje internetowe”, „Prywatność”, a następnie „Zaawansowane”;
            w przeglądarce Firefox (wersja 24) możesz zablokować wszystkie pliki cookie, klikając „Narzędzia”, „Opcje”, „Prywatność”, wybierając „Użyj ustawień niestandardowych dla historii” z menu rozwijanego i odznaczając „Akceptuj ciasteczka z witryn”; i
            w Chrome (wersja 29) możesz zablokować wszystkie pliki cookie, otwierając menu „Dostosuj i kontroluj”, klikając „Ustawienia”, „Pokaż ustawienia zaawansowane” i „Ustawienia treści”, a następnie wybierając „Blokuj witrynom ustawianie dowolnych danych” pod nagłówkiem „Pliki cookie”.
            Zablokowanie wszystkich plików cookie będzie miało negatywny wpływ na możliwość korzystania z wielu stron internetowych. Jeśli zablokujesz pliki cookie, nie będziesz mógł korzystać z wszystkich funkcji naszej strony.

            Możesz usunąć pliki cookie już zapisane na komputerze — na przykład:
            w przeglądarce Internet Explorer (wersja 10) należy ręcznie usunąć pliki cookie (instrukcje można znaleźć na stronie http://support.microsoft.com/kb/278835 );
            w przeglądarce Firefox (wersja 24) możesz usunąć pliki cookie, klikając „Narzędzia”, „Opcje” i „Prywatność”, a następnie wybierając „Użyj ustawień niestandardowych dla historii”, klikając „Pokaż pliki cookie”, a następnie klikając „Usuń wszystkie pliki cookie”; i
            w przeglądarce Chrome (wersja 29) można usunąć wszystkie pliki cookie, otwierając menu „Dostosuj i kontroluj”, klikając „Ustawienia”, „Pokaż ustawienia zaawansowane” i „Wyczyść dane przeglądarki”, a następnie wybierając „Usuń pliki cookie i dane innych stron i wtyczek” przed kliknięciem „Wyczyść dane
            przeglądania”.
            Usunięcie plików cookie będzie miało negatywny wpływ na użyteczność wielu stron internetowych.'
        ]

    ],

    "privacyPolicy" => [
        "name" => "Polityka prywatności",
        "list" => [
            "1" => "<b style='font-weight: bold' >Wprowadzenie</b> <br>
Prywatność odwiedzających naszą stronę internetową jest dla nas bardzo ważna i dokładamy wszelkich starań, aby ją chronić. Niniejsza polityka wyjaśnia, co robimy z Twoimi danymi osobowymi.
Zgoda na korzystanie z plików cookie zgodnie z warunkami niniejszej polityki podczas pierwszej wizyty na naszej stronie pozwala nam na korzystanie z plików cookie przy każdej kolejnej wizycie na naszej stronie. ",

            "2" => "<b style='font-weight: bold' >Zbieranie danych osobowych</b> <br>

Następujące rodzaje danych osobowych mogą być gromadzone, przechowywane i wykorzystywane:
<ul>
<li>informacje o komputerze, w tym adres IP, lokalizacja geograficzna, typ i wersja przeglądarki oraz system operacyjny;</li>
<li>informacje o Twoich wizytach i korzystaniu z tej witryny, w tym źródło odesłań, długość wizyty, wyświetlenia stron i ścieżki nawigacji w witrynie;</li>
<li>informacje, takie jak adres e-mail, które podajesz podczas rejestracji w naszej witrynie internetowej;</li>
<li>informacje wprowadzane podczas tworzenia profilu w naszej witrynie - np. nick oraz e-mail;</li>
<li>informacje, adres e-mail, które podajesz w celu skonfigurowania subskrypcji naszych e-maili lub biuletynów;</li>
<li>informacje wprowadzane podczas korzystania z usług na naszej stronie internetowej;</li>
<li>informacje, które są generowane podczas korzystania z naszej strony internetowej, w tym kiedy, jak często i w jakich okolicznościach z niej korzystasz;</li>
<li>informacje dotyczące wszystkiego, co kupujesz, usług, z których korzystasz lub transakcji dokonywanych za pośrednictwem naszej strony internetowej, w tym imię i nazwisko, adres, numer telefonu, adres e-mail i dane karty kredytowej;</li>
<li>informacje publikowane na naszej stronie internetowej z zamiarem opublikowania ich w Internecie, w tym nazwa użytkownika, zdjęcia profilowe i treść umieszczanych postów;</li>
<li>informacje zawarte we wszelkiej korespondencji przesyłanej do nas e-mailem lub za pośrednictwem naszej strony internetowej, w tym treści komunikacyjne i metadane;</li>
wszelkie inne dane osobowe, które nam przesyłasz.</li>
<li>Zanim ujawnisz nam dane osobowe innej osoby, musisz uzyskać zgodę tej osoby na ujawnienie i przetwarzanie tych danych osobowych zgodnie z niniejszymi zasadami</li>
</ul> ",
            "3" => "<b style='font-weight: bold' >Korzystanie z Twoich danych osobowych</b> <br>

Dane osobowe przesłane do nas za pośrednictwem naszej strony internetowej będą wykorzystywane do celów określonych w niniejszej polityce lub na odpowiednich stronach witryny. Możemy wykorzystywać Twoje dane osobowe do celu:
<ul>
<li>administrowania naszą stroną internetową i biznesem;</li>
<li>personalizowania naszej strony internetowej dla Ciebie;</li>
<li>umożliwienia korzystania z usług dostępnych na naszej stronie internetowej;</li>
<li>świadczenia usług zakupionych za pośrednictwem naszej strony internetowej;</li>
<li>wysyłania do ciebie wyciągów, faktur i przypomnień o płatnościach oraz odbioru płatności od Ciebie;</li>
<li>przesyłania niemarketingowej komunikacji handlowej;</li>
<li>wysyłania powiadomień e-mail, o które prosiłeś;</li>
<li>wysyłania naszego newslettera e-mail, jeśli o to poprosiłeś (możesz nas w każdej chwili poinformować, jeśli nie chcesz już otrzymywać newslettera poprzez mail lub pole wyboru w profilu na naszej stronie);</li>
<li>dostarczania stronom trzecim informacji statystycznych o naszych użytkownikach (ale te osoby trzecie nie będą w stanie zidentyfikować żadnego konkretnego użytkownika na podstawie tych informacji);</li>
<li>zajmowania się zapytaniami i skargami składanymi przez Ciebie lub dotyczącymi Ciebie w związku z naszą witryną;</li>
<li>zapewnienia bezpieczeństwa naszej strony internetowej i zapobieganie oszustwom;</li>
weryfikacji zgodności z warunkami korzystania z naszej strony internetowej (w tym monitorowanie prywatnych wiadomości wysyłanych za pośrednictwem naszej prywatnej usługi przesyłania wiadomości);</li>
<li>i innych zastosowań.</li>
</ul>
Jeśli prześlesz dane osobowe do publikacji w naszej witrynie, opublikujemy je i wykorzystamy w inny sposób zgodnie z udzieloną nam licencją.

Twoje ustawienia prywatności mogą być wykorzystane do ograniczenia publikacji Twoich informacji na naszej stronie internetowej i mogą być dostosowane za pomocą kontroli prywatności na stronie.

Nie będziemy bez Twojej wyraźnej zgody przekazywać danych osobowych stronom trzecim, lub jakimkolwiek innym związanym z nimi stronom trzecim, w celach marketingu bezpośredniego. ",
            "4" => "<b style='font-weight: bold' >Ujawnianie danych osobowych</b> <br>

Możemy ujawniać dane osobowe użytkownika wszelkim naszym pracownikom, członkom kadry kierowniczej, przedstawicielom, dostawcom lub podwykonawcom, o ile jest to niezbędne do celów określonych w niniejszych zasadach „Polityka Prywatności”.

Możemy ujawniać dane osobowe wszelkim członkom naszej grupy spółek (tzn. spółkom zależnym, głównej spółce holdingowej i jej spółkom zależnym), o ile jest to niezbędne do celów określonych w niniejszych „Polityka Prywatności”.

Możemy ujawniać Twoje dane osobowe:
<ul>
<li>w zakresie, w jakim jest to wymagane przepisami prawa;</li>
<li>w związku z trwającymi lub potencjalnymi postępowaniami prawnymi;</li>
<li>w celu ustanowienia, wyegzekwowania lub obrony naszych praw (wliczając w to udostępnienie informacji innym podmiotom w celu przeciwdziałania oszustwom);
nabywcy (lub potencjalnemu nabywcy) jakiejkolwiek działalności lub aktywów, które sprzedajemy (lub rozważamy); i
wszelkim osobom, które wedle naszej zasadnej opinii mogą wystąpić do sądu lub innego właściwego organu o ujawnienie takich danych osobowych, jeśli wedle zasadnej rozsądnej opinii, istnieje duże prawdopodobieństwo, że taki sąd lub organ nakaże ujawnienie takich danych osobowych</li>
</ul>
Z wyjątkiem postanowień zawartych w niniejszych zasadach „Polityka prywatności” nie będziemy udostępniać osobom trzecim informacji dotyczących użytkownika.",

            "5" => "<b style='font-weight: bold' >Międzynarodowe transfery danych</b> <br>

Informacje, które gromadzimy mogą być przechowywane i przetwarzane w każdym z krajów, w którym prowadzimy działalność i mogą być przesyłane pomiędzy tymi krajami w celu umożliwienia wykorzystania informacji zgodnie z niniejszymi zasadami „Polityka prywatności”.
Informacje, które zbieramy, mogą być przekazywane do następujących krajów, które nie mają przepisów o ochronie danych równoważnych z obowiązującymi w Europejskim Obszarze Gospodarczym: Stanów Zjednoczonych Ameryki, Rosji, Japonii, Chin, Indii i inne.
Dane osobowe, które publikujesz na naszej stronie internetowej lub przesyłasz do publikacji na naszej stronie internetowej, mogą być dostępne za pośrednictwem Internetu na całym świecie. Nie możemy zapobiec wykorzystywaniu lub niewłaściwemu wykorzystaniu takich informacji przez inne osoby.
Wyraźnie zgadzasz się na przekazywanie danych osobowych opisanych w tej sekcji 'Zachowywanie danych osobowych'. ",

            "6" => "<b style='font-weight: bold' >Zachowywanie danych osobowych</b> <br>

W Niniejszej Sekcji 'Zachowywanie danych osobowych' określiliśmy nasze zasady i procedury dotyczące zatrzymywania danych, które mają na celu zapewnienie przestrzegania naszych zobowiązań prawnych w zakresie zachowywania i usuwania danych osobowych.
Dane osobowe, które przetwarzamy w dowolnym celu lub celach, nie będą przechowywane dłużej niż jest to konieczne do tego celu lub tych celów.
Dane są usuwane na wniosek porpzez e-mail lub odpowieni przycisk w profilu. Zazwyczaj usuwamy wszystie dane osobowe zgłaszającego do 11 dni roboczych. Poprzez przycisk usuwane są natychmiastowo.
Niezależnie od innych postanowień niniejszej sekcji zachowamy dokumenty (w tym dokumenty elektroniczne) zawierające dane osobowe:
w zakresie, w jakim jest to wymagane przepisami prawa;
jeśli uważamy, że dokumenty mogą mieć znaczenie dla wszelkich toczących się lub przyszłych postępowań sądowych; i
w celu ustanowienia, wyegzekwowania lub obrony naszych praw (wliczając w to udostępnienie informacji innym podmiotom w celu przeciwdziałania oszustwom).",

            "7" => "<b style='font-weight: bold' >Bezpieczeństwo danych osobowych</b> <br>

Podejmiemy zasadne techniczne i organizacyjne środki ostrożności w celu przeciwdziałania utracie, nadużyciu lub zmianie danych osobowych użytkownika.
Będziemy przechowywać wszystkie dane osobowe, które podasz na naszych bezpiecznych serwerach (chronionych hasłem i zaporą).
Wszystkie elektroniczne transakcje finansowe zawierane za pośrednictwem naszej strony internetowej będą chronione technologią szyfrowania.
Użytkownik przyjmuje do wiadomości, że transmisja informacji przez Internet jest potencjalnie niebezpieczna i bezpieczeństwo przesłanych w ten sposób danych nie jest gwarantowane.
Jesteś odpowiedzialny za zachowanie poufności hasła używanego do uzyskania dostępu do naszej strony internetowej; nie poprosimy Cię o podanie hasła (z wyjątkiem sytuacji, gdy logujesz się na naszej stronie internetowej). ",

            "8" => "<b style='font-weight: bold' >Nowelizacje</b> <br>

Niniejsze zasady mogą być okresowo aktualizowane poprzez zamieszczenie w naszej witrynie ich nowej wersji. Należy od czasu do czasu sprawdzać tę stronę, aby upewnić się, że rozumiesz wszelkie zmiany w tych zasadach. Możemy powiadomić Cię o zmianach w niniejszej polityce za pośrednictwem poczty elektronicznej lub prywatnego systemu przesyłania wiadomości na naszej stronie internetowej. W przypadku modyfikacji większości polityki prywatności, zostanie również wysłany mail powiadamiający o takiej zmianie.",

            "9" => "<b style='font-weight: bold' >Twoje prawa</b> <br>

Użytkownik może zażądać udostępnienia jego danych osobowych, które są przechowywane. Udostępnienie takich danych wiąże się z:

przesłaniem prośby na maila kontaktowego i
dostarczenie skanu dowodu tożsamości.
Możemy na Twoje żądanie ukrywać dane osobowe, w zakresie dozwolonym przez prawo.

Możesz w dowolnym momencie poinstruować nas, aby nie przetwarzać danych osobowych w celach marketingowych.

W praktyce zazwyczaj albo z góry wyraźnie wyrażasz zgodę na wykorzystanie przez nas Twoich danych osobowych w celach marketingowych, albo zapewnimy Ci możliwość rezygnacji z udostępniania Twoich danych osobowych w celach marketingowych.",

            "10" => "<b style='font-weight: bold' >Strony Internetowe osób trzecich</b> <br>

Nasza strona internetowa zawiera hiperłącza do stron internetowych osób trzecich oraz szczegółowe informacje na ich temat. Nie mamy kontroli i nie ponosimy odpowiedzialności za politykę prywatności i praktyki osób trzecich.",

            "11" => "<b style='font-weight: bold' >Aktualizacja informacji</b> <br>

Prosimy o informację w przypadku konieczności skorygowania lub aktualizowania danych osobowych, w których posiadaniu jesteśmy. ",

            "12" => "<b style='font-weight: bold' >Ciasteczka</b> <br>

Nasza strona internetowa korzysta z plików cookie. Plik cookie to plik zawierający identyfikator (ciąg liter i cyfr), który jest wysyłany przez serwer internetowy do przeglądarki internetowej i przechowywany przez przeglądarkę. Dane identyfikacyjne są ponownie przesyłane na serwer za każdym razem, gdy przeglądarka wyśle żądanie otwarcia strony znajdującej się na serwerze. Pliki cookie mogą być „trwałymi” lub „sesyjnymi” plikami cookie: trwałe pliki cookie będą przechowywane przez przeglądarkę internetową i pozostaną ważne do ustalonej daty wygaśnięcia, chyba że użytkownik usunie je przed datą wygaśnięcia; sesyjny plik cookie wygasa z końcem sesji użytkownika, gdy przeglądarka internetowa jest zamykana. Pliki cookie zazwyczaj nie zawierają żadnych informacji identyfikujących użytkownika, ale dane osobowe, które przechowujemy na Twój temat, mogą być powiązane z informacjami przechowywanymi w plikach cookie i uzyskiwanymi z nich. Używamy zarówno sesyjnych, jak i trwałych ciasteczek na naszej stronie. Więcej informacji dotyczących ciasteczek można znaleźć w 'Polityka prywatności cookie'.
"
        ],
    ],
    "contact" => [
        "name" => "Kontakt",
        "place" => "Polska"
    ]

];
