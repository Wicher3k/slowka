<?php
return [
    "meta" => [
        "description" => "Dzięki My flashcard Place za pomocą gotowych lub własnych fiszek nauka języków i innych nigdy nie była tak łatwa."
    ],
    "areYouSureToDeactivateTheAccount" => "Czy na pewno chcesz dezaktywować konto?",
    "accountDactivation" => "Dezaktywacja konta",
    "categoryName" => "Nazwa kategorii",
    "categories" => "Kategorie",
    "categoriesAndXothers" => "{1} i :count inny|[2,4] i :count inne|[5,*] i :count innych",
    "changePassword" => "Zmiana hasła",
    "confirmPassword" => "Potwierdź hasło",
    "confirmPasswordBeforeContinuing" => "Proszę potwiedzić hasło przed kontynuowaniem",
    "cookieNavbar" => "Strona korzysta z plików cookies. Korzystając z naszej strony, potwierdzasz, że przeczytałeś i rozumiesz naszą :cookiePolicy oraz :privacyPolicy.",
    "currentPassword" => "Aktualne hasło",
    "current" => "obecny",
    "deactivationText" => "Dezaktywacja konta wiąże się z całkowitym usunięciem wszystkich wprowadzonych danych. Nie będzie możliwe ponowne zalogowanie się na to konto w aplikacji.",
    "deactivationConfirmation" => "Potwierdzenie dezaktywacji",
    "E-MailAddress" => "Adres E-Mail",
    "flashcards" => "Fiszki",
    "forgetYourPassword" => "Zapomniałeś hasła?",
    "from" => "Z",

    "goodJob" => "Gratulacje",
    "history" => "Historia",
    "language" => "Język",
    "listOfWords" => "Lista słówek",
    "login" => "Zaloguj",
    "logout" => "Wyloguj",
    "newPassword" => "Nowe hasło",
    "notification" =>[
        "chooseAtLeastOneLanguage" => "Wybierz przynajmniej jeden język",
        "chooseAtLeastTwoLanguages" => "Wybierz conajmniej dwa języki",
        "deletedSuccessfully" => "Usunięto pomyślnie",
        "savedSuccessfully" => "Zapisano pomyślnie"
    ],
    "myCategories" => "Moje kategorie",
    "myflashcards" => "Moje fiszki",
    "onPage" => "W nowej karcie przeglądarki",
    "password" => "Hasło",
    "privacyPolicy" => "Polityka prywatności",
    "myProfile" => "Mój Profil",
    "register" => "Zarejestruj się",
    "registration" =>
    [
        "newsletter" => 'Wyrażam zgodę na przetwarzanie moich danych osobowych w celu otrzymywania od '.config("app.name").' informacji handlowej oraz marketingowej za pomocą środków komunikacji elektronicznej (email).',
        "regulations" => 'Potwierdzam, że zapoznałam(em) się z treścią :cookiePolicy oraz :privacyPolicy',
    ],

    "repeatNewPassword" => "Powtórz nowe hasło",
    "rememberMe" => "Zapamiętaj mnie",
    "resetPassword" => "Resetuj hasło",
    "shortcuts" => [
        "keyboard" => "Skróty klawiszowe",
        "touch" => "Skróty - Przeciągnij",
    ],
    "selectAListingMethod" => "Wybierz metodę wylistowania",
    "selectedCategories" => "Wybrane kategorie",
    "selectCategories" => "Wybierz kategorie",
    "selectLanguages" => "Wybierz języki",
    "to" => "Na",
    "username" => 'Login',
    "usersCategories" => 'Kategorie użytkownika :login',
    "vacabularyLeft" => "Pozostało <span id='start-left'> :all </span> z :all",
    "verifyMail" => [
        "verifyYourEmailAddress" => "Zweryfikuj swój adres email",
        "linksended" => "Świeżutki link weryfikacyjny został wysłany na Twój adres e-mail",
        "linkcheck1" => "Przed kontynuowaniem sprawdź adres e-mail, aby uzyskać link weryfikacyjny.",
        "linkcheck2" => "Jeśli nie otrzymałeś wiadomości e-mail.",
        "linksend" => "Kliknij tutaj, aby wysłać kolejny",
        "register1" => "Zalogowano!",
        "register2" => "Na Twoj adres e-mail został wysłany link weryfikacyjny.",
    ]


];
