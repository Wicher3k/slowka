<?php
return [
    "add" => 'Dodaj',
    "cancel" => 'Anuluj',
    "changePassword" => 'Zmiana hasła',
    "check" => "Sprawdź",
    "createNewCategory" => "Stwórz nową categorię z fiszkami!",
    "delete" => "Usuń",
    "edit" => "Edytuj",
    "forgotYourPassword" => 'Zapomniałeś hasła?',
    "good" => "Dobrze",
    "login" => 'Login',
    "newCategories" => 'Nowe kategorie',
    "pool" => 'Pula',
    "register" => 'Register',
    "retry" => 'Powtórz',
    "return2MainPage" => 'Powrót do strony głównej',
    "save" => 'Zapisz',
    "sendPasswordResetLink" => 'Wyślij link z resetowaniem hasła',
    "start" => 'Rozpocznij',
    "toThePot" => 'Do puli',
    "updateMyProfile" => 'Zaktualizuj mój profil',


];








