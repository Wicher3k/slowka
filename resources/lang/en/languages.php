<?php
return [

    'pl' => 'Polish',
    'en' => 'English',
    'ch' => 'Chinese (pinyin)',
    'ch_2' => 'Chinese (simplified)',
    'ko' => 'Korean',

];
