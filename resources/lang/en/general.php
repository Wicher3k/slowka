<?php
return [
    "meta" => [
        "description" => "With My Flashcard Place, using pre-made or custom flashcards has never been easier to learn languages and others."
    ],
    "areYouSureToDeactivateTheAccount" => "Are you sure to deactivate the account?",
    "accountDactivation" => "Account dactivation",
    "categoryName" => "Category name",
    "categories" => "Categories",
    "categoriesAndXothers" => "[0,*]and :count others",
    "changePassword" => "Change Password",
    "confirmPassword" => "Confirm Password",
    "confirmPasswordBeforeContinuing" => "Please confirm your password before continuing",
    "cookieNavbar" => "This site uses cookies. By using our site, you acknowledge that you have read and understand our :cookiePolicy and :privacyPolicy",
    "currentPassword" => "Current Password",
    "current" => "current",
    "deactivationText" => "Deactivating the account involves the complete deletion of all entered data. It will not be possible to log into this account in the application again.",
    "deactivationConfirmation" => "Deactivation confirmation",
    "E-MailAddress" => "E-Mail Address",
    "flashcards" => "Flashcards",
    "forgetYourPassword" => "Forgot Your Password?",
    "from" => "From",

    "goodJob" => "Good Job",
    "history" => "History",
    "language" => "Language",
    "listOfWords" => "Word list",
    "login" => "Login",
    "logout" => "Logout",
    "newPassword" => "New Password",
    "myCategories" => "My Categories",
    "myflashcards" => "My Flashcards",
    "notification" => [
        "chooseAtLeastOneLanguage" => "Choose at least one language",
        "chooseAtLeastTwoLanguages" => "Choose at least two languages",
        "deletedSuccessfully" => "Deleted Successfully",
        "savedSuccessfully" => "Saved successfully"
    ],
    "onPage" => "In new tab",
    "password" => "Password",
    "privacyPolicy" => "Privacy Policy",
    "myProfile" => "My Profile",
    "register" => "Register",
    "registration" =>
    [
        "newsletter" => 'I consent to the processing of my personal data in order to receive commercial and marketing information from '.config("app.name").' by means of electronic communication (email).',
        "regulations" => 'I confirm that I have read the content :cookiePolicy and :privacyPolicy',
    ],

    "repeatNewPassword" => "Repeat New Password",
    "rememberMe" => "Remember Me",
    "resetPassword" => "Reset Password",

    "selectAListingMethod" => "Select a listing method",
    "selectedCategories" => "Selected categories",
    "selectCategories" => "Select categories",
    "selectLanguages" => "Select languages",
    "shortcuts" => [
        "keyboard" => "Keyboard shortcuts",
        "touch" => "Drag shortcuts",
    ],
    "to" => "To",
    "vacabularyLeft" => "<span id='start-left'> :all </span> of :all left",
    "username" => 'Login',
    "usersCategories" => ':login\'s categories',
    "verifyMail" => [
        "verifyYourEmailAddress" => "Verify Your Email Address",
        "linksended" => "A fresh verification link has been sent to your email address",
        "linkcheck1" => "Before proceeding, please check your email for a verification link.",
        "linkcheck2" => "If you did not receive the email.",
        "linksend" => "Click here to request another",
        "register1" => "You are logged in!",
        "register2" => "A verification link has been sent to your e-mail address.",
    ]
];
