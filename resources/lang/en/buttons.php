<?php
return [
    "add" => 'Add',
    "cancel" => 'Cancel',
    "changePassword" => 'Change Password',
    "check" => "Check",
    "createNewCategory" => "Create a new category with flashcards!",
    "delete" => "Delete",
    "edit" => "Edit",
    "forgotYourPassword" => 'Forgot Your Password?',
    "good" => "Good",
    "login" => 'login',
    "newCategories" => 'New categories',
    "pool" => 'Pool',
    "register" => 'Register',
    "retry" => 'Retry',
    "return2MainPage" => 'Back to main page',
    "save" => 'Save',
    "sendPasswordResetLink" => 'Send Password Reset Link',
    "start" => 'Start',
    "toThePot" => 'To the pot',
    "updateMyProfile" => 'Update My Profile',

];








