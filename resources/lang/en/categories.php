<?php

return [
    "colors" => "Colors",
    "numbers" => "Numbers",
    "dateAndTime" => "Date and Time",

    // "food" => [
    "fruits" => "Food - Fruits",
    "vegetables" => "Food - Vegetables",
    "dairy" => "Food - Dairy",
    "drinks" => "Food - Drinks",
    "extras" => "Food - Extras",
    "other" => "Food - Others",

    // ]
    "50 Popular Verbs" => "50 Popular Verbs",
    "Animals (Basics)" => "Animals (Basics)",
    "Animals (Advanced)" => "Animals (Advanced)",

    "Chinese - Lesson 1a - Words" => "Chinese - Lesson 1a - Words",
    "Chinese - Lesson 1a - Sentences" => "Chinese - Lesson 1a - Sentences",
    "Chinese - Lesson 1b - Words" => "Chinese - Lesson 1b - Words",
    "Chinese - Lesson 1b - Sentences" => "Chinese - Lesson 1b - Sentences",
    "Chinese - Lesson 2a - Words" => "Chinese - Lesson 2a - Words",
    "Chinese - Lesson 2a - Sentences" => "Chinese - Lesson 2a - Sentences",
    "Chinese - Lesson 2b - Words" => "Chinese - Lesson 2b - Words",
    "Chinese - Lesson 2b - Sentences" => "Chinese - Lesson 2b - Sentences",

];
