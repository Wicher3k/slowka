<?php
return [

    "cookie" => [
        "name" => "Cookie Policy",
        "list" => [
            "1" => 'The website uses cookies.',
            "2" => 'Cookie files (so-called "cookies") are IT data, in particular text files, which are stored on the User\'s end device.',
            "3" => 'Cookies improve the use of the Website\'s pages. Cookies usually contain the name of the website from which they originate, their storage time on the end device and a unique number',
            "4" => 'The entity placing cookies on the Website User\'s end device and accessing them is the Website Operator.',
            "5" => 'Cookies are used to:
                         adapt the content of the Website pages to the User\'s preferences and optimize the use of websites; in particular, these files allow to recognize the Website User\'s device and properly display the website, tailored to his individual needs;
                         creating statistics that help understand how Website Users use websites, which allows improving their structure and content;
                         maintaining the Website User\'s session, thanks to which the User does not have to complete the user\'s individual settings on the website after returning to the website;
                         changes in the appearance or version of the website;
                         cache management to optimize user page display time.',
            "6" => 'The Website uses two basic types of cookies: session cookies and persistent cookies. Session cookies are temporary files that are stored on the User\'s end device until logging out, leaving the website or turning off the software (web browser). Persistent cookies are stored on the User\'s end device for the time specified in the cookie parameters or until they are deleted by the User.',
            "7" => 'The Website uses the following types of cookies: <br>
            • "necessary" cookies, enabling the use of services available on the Website, e.g. authentication cookies used for services that require authentication on the Website; cookies used to ensure security, e.g. used to detect fraud in the field of authentication on the Website; <br>
            • "performance" cookies, enabling the collection of information on how to use the Website\'s pages; <br>
            • "functional" cookies, enabling "remembering" the settings selected by the User and personalizing the User\'s interface, e.g. in terms of the language or region of the User\'s origin, font size, website appearance, etc .; <br>
            • "advertising" cookies, enabling users to provide advertising content more tailored to their interests.',
            "8" => 'In many cases, the software used for browsing websites (web browser) by default allows the storage of cookies on the User\'s end device. Website Users can change cookie settings at any time. These settings can be changed in particular in such a way as to block the automatic handling of cookies in the web browser settings or to inform about them every time they are placed on the Website User\'s device. Detailed information about the possibilities and ways of handling cookies is available in the software (web browser) settings.',
            "9" => 'The Website operator informs that restrictions on the use of cookies may affect some of the functionalities available on the Website.',
            "10" => 'Cookies placed on the Website User\'s end device may also be used by advertisers and partners cooperating with the Website operator.',
            "11" => '
            The names of the cookies we use on our website and the purposes for which they are used are set out below:
                we use Google Analytics to recognize the computer when the user visits the website / follows the users while browsing the website / enables the use of the basket on the website / improve the usability of the website / analyze the use of the website / administer the website / manage the website / prevent fraud and improve the website security website / personalize the website for each user / target advertisements that may be of particular interest to specific users;
                Most browsers allow you to refuse to accept cookies - for example:
                in Internet Explorer (version 10) you can block cookies using the cookie handling override settings available by clicking "Tools", "Internet Options", "Privacy" and then "Advanced";
                in Firefox (version 24) you can block all cookies by clicking "Tools", "Options", "Privacy", selecting "Use custom settings for history" from the drop-down menu and unchecking "Accept cookies from sites"; and
                in Chrome (version 29) you can block all cookies by opening the "Customize and control" menu, clicking "Settings", "Show advanced settings" and "Content settings", and then selecting "Block sites from setting any data" under the "Files cookie".
                Blocking all cookies will have a negative impact on your ability to use multiple websites. If you block cookies, you will not be able to use all the features of our website.

                You can delete cookies already stored on your computer - for example:
                in Internet Explorer (version 10) you must manually delete cookies (instructions can be found at http://support.microsoft.com/kb/278835);
                in Firefox (version 24) you can delete cookies by clicking "Tools", "Options" and "Privacy", then selecting "Use custom settings for history", clicking "Show cookies" and then clicking "Delete all files" cookie"; and
                in Chrome (version 29) you can delete all cookies by opening the "Customize and control" menu, clicking "Settings", "Show advanced settings" and "Clear browser data", then selecting "Delete cookies and other site data and Plugins "before clicking" Clear data
                browsing ".
                Deleting cookies will have a negative impact on the usability of many websites.'
        ]

    ],

    "privacyPolicy" => [
        "name" => "Privacy Policy",
        "list" => [
            "1" => "<b style='font-weight: bold' >Introduction</b> <br>
            The privacy of our website visitors is very important to us and we are committed to protecting it. This policy explains what we do with your personal data.
            By consenting to the use of cookies in accordance with the terms of this policy on your first visit to our website, we are permitted to use cookies on each subsequent visit to our website.",

"2" => "<b style='font-weight: bold' >Collection of personal data</b> <br>

The following types of personal data may be collected, stored and used:
<ul>
<li> information about your computer, including your IP address, geographic location, browser type and version, and operating system; </li>
<li> information about your visits to and use of this site, including referral source, length of visit, page views, and site navigation paths; </li>
<li> information such as the e-mail address that you provide when registering on our website; </li>
<li> information entered when creating a profile on our website - e.g. nickname and e-mail; </li>
<li> information, e-mail address that you provide to configure subscriptions to our e-mails or newsletters; </li>
<li> information that you enter when you use the services on our website; </li>
<li> information that is generated when you use our website, including when, how often and under what circumstances you use it; </li>
<li> information regarding everything you buy, services you use or transactions made through our website, including your name, address, telephone number, email address, and credit card details; </li>
<li> information posted on our website with the intention of publishing it on the internet, including username, profile pictures and the content of the posted posts; </li>
<li> information contained in any correspondence you send to us by e-mail or through our website, including communication content and metadata; </li>
any other personal information that you submit to us. </li>
<li> Before you disclose to us the personal information of another person, you must obtain that person's consent to both the disclosure and the processing of that personal information in accordance with this policy </li>
</ul> ",
"3" => "<b style='font-weight: bold' >Use of your personal data</b> <br>

Personal data submitted to us through our website will be used for the purposes specified in this policy or on the relevant pages of the website. We may use your personal data for the purpose of:
    <ul>
    <li> administering our website and business; </li>
    <li> personalize our website for you; </li>
    <li> enabling the use of services available on our website; </li>
    <li> to provide services purchased through our website; </li>
    <li> send you statements, invoices and payment reminders, and collect payments from you; </li>
    <li> sending you non-marketing commercial communications; </li>
    <li> send email notifications you requested; </li>
    <li> sending our e-mail newsletter, if you have requested it (you can inform us at any time if you no longer want to receive the newsletter via e-mail or the checkbox in the profile on our website); </li>
    <li> provide third parties with statistical information about our users (but those third parties will not be able to identify any specific user from that information); </li>
    <li> deal with inquiries and complaints made by or about you relating to our website; </li>
    <li> to keep our website secure and to prevent fraud; </li>
    verifying compliance with the terms of use of our website (including monitoring private messages sent through our private messaging service); </li>
    <li> and other uses. </li>
    </ul>
    If you submit personal information for publication on our site, we will publish it and otherwise use it in accordance with the license granted to us.

    Your privacy settings can be used to restrict the publication of your information on our website and can be adjusted using the website privacy controls.

    We will not pass your personal data to third parties or any other related third parties for direct marketing purposes without your express consent.",
"4" => "<b style='font-weight: bold' >Disclosure of personal data</b> <br>

We may disclose your personal information to any of our employees, officers, agents, suppliers or subcontractors as necessary for the purposes set out in this „Privacy Policy”.

We may disclose personal information to any members of our group of companies (ie subsidiaries, main holding company and its subsidiaries) as necessary for the purposes set out in this Privacy Policy.

We may disclose your personal data:
<ul>
<li> to the extent required by law; </li>
<li> in connection with any ongoing or potential legal proceedings; </li>
<li> in order to establish, exercise, or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk);
the buyer (or prospective acquirer) of any business or asset that we sell (or are contemplating); and
to any person who we reasonably believe may apply to a court or other competent authority for disclosure of such personal information where, in our reasonable opinion, such court or authority is reasonably likely to order disclosure of such personal information </li>
</ul>
Except as provided in this „Privacy Policy”, we will not share your information with third parties.",

"5" => "<b style='font-weight: bold' >International data transfers</b> <br>

The information we collect may be stored and processed in any of the countries in which we do business and may be transferred between these countries to enable information to be used in accordance with this „Privacy Policy”.
The information we collect may be transferred to the following countries that do not have data protection laws equivalent to those in force in the European Economic Area: United States of America, Russia, Japan, China, India and others.
Personal information that you publish on our website or submit for publication on our website may be available, via the internet, around the world. We cannot prevent the use or misuse of such information by others.
You expressly consent to the transfers of personal data described in this section 'Retaining Personal Data'. ",

"6" => "<b style='font-weight: bold' >Retention of personal data</b> <br>

In this 'Retention of Personal data' section, we have set out our data retention policies and procedures to ensure compliance with our legal obligations to retain and delete personal information.
Personal data that we process for any purpose or purposes will not be kept for longer than is necessary for that purpose or those purposes.
The data is deleted on request by e-mail or the appropriate button in the profile. We usually delete all personal data of the applicant within 11 business days. They are deleted immediately via a button.
Notwithstanding anything else in this section, we will retain documents (including electronic documents) containing personal information:
to the extent that it is required by law;
if we believe that the documents may be relevant to any ongoing or prospective legal proceedings; and
in order to establish, exercise, or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk).",

"7" => "<b style='font-weight: bold' >Security of personal data</b> <br>

We will take reasonable technical and organizational precautions to prevent the loss, misuse or alteration of your personal data.
We will store all personal information you provide on our secure servers (password and firewall protected).
All electronic financial transactions made through our website will be protected by encryption technology.
The user acknowledges that the transmission of information over the internet is potentially insecure and the security of the data transmitted in this way is not guaranteed.
You are responsible for maintaining the confidentiality of the password used to access our website; we will not ask you for your password (except when you log into our website). ",

"8" => "<b style='font-weight: bold' >Amendments</b> <br>

We may update this policy from time to time by posting a new version on our site. You should check this page from time to time to ensure you understand any changes to this policy. We may notify you of changes to this policy via email or the private messaging system on our website. If most of the privacy policy is modified, an e-mail will also be sent to notify you of such change.",

"9" => "<b style='font-weight: bold' >Your rights</b> <br>

The user may request access to his personal data that is stored. Providing such data is related to:

sending a request to a contact e-mail and
providing a scan of an identity card.
We may hide personal data at your request, to the extent permitted by law.

You can instruct us at any time not to process your personal data for marketing purposes.

In practice, you usually either expressly consent in advance to our use of your personal data for marketing purposes, or we will provide you with the option to opt-out of sharing your personal data for marketing purposes.",

"10" => "<b style='font-weight: bold' >Third party websites</b> <br>

Our website contains hyperlinks to third party websites and detailed information about them. We have no control over and are not responsible for the privacy policies and practices of third parties.",

"11" => "<b style='font-weight: bold' >Information update</b> <br>

Please let us know if we need to correct or update the personal data we hold about you. ",

"12" => "<b style='font-weight: bold' >Cookies</b> <br>

Our website uses cookies. A cookie is a file containing an identifier (a string of letters and numbers) that is sent by the web server to the web browser and stored by the browser. The identification data is sent back to the server each time the browser requests to open a page on the server. Cookies can be 'persistent' or 'session' cookies: persistent cookies will be stored by your web browser and will remain valid until the expiration date you set, unless you delete them before the expiration date; the session cookie expires at the end of the user session when the web browser is closed. Cookies typically do not contain any information that identifies you, but the personal information we store about you may be linked to information stored in and obtained from cookies. We use both session and persistent cookies on our website. More information on cookies can be found in the 'Cookie Privacy Policy'.

<br>
<br>
<i>This „Privacy Policy” has been translated from the original Polish version of <a href='/pl/cookiePolicy'>„Privacy Policy” </a></i>

"
        ]
    ],

    "contact" => [
        "name" => "Contact",
        "place" => "Poland"
    ]

];
