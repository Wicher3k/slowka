<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public $timestamps = false;

    public static $languageModel = NULL;


    public static function byAcronym()
    {
        if (!isset(self::$languageModel)) {
            self::createModel();
        }
        return  self::$languageModel->keyBy('acronym')->all();
    }

    public static function byId()
    {
        if (!isset(self::$languageModel)) {
            self::createModel();
        }
        return  self::$languageModel->keyBy('id')->all();
    }

    private static function createModel(){
        self::$languageModel = self::get();
    }
}
