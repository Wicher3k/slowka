<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flashcard extends Model
{
    public function categories()
    {
        return $this->belongsToMany('App\Categories')->withTimestamps();
    }


    public function wordsRelation()
    {
        return $this->hasMany(
            'App\Word',
            'flashcard_id',
            'id'
        );
    }

    public function getwordsAttribute() // !! use $flashcard -> words !! not $flashcard->wordsRelation
    {
        return $this->wordsRelation->keyby("language_id")->all();
    }
}
