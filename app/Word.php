<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    public function flashcard()
    {
        return $this->belongsTo(
            'App\Flashcard'
        );
    }

    public function language()
    {
        return $this->belongsTo(
            'App\Language'
        );
    }

    public function wordinfos()
    {
        return $this->hasOne('App\WordInfo');
    }
}
