<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use App;
use Config;
use Session;
class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function changeLocale($request)
    {
        if ($request->method() === 'GET') {
            // dd($request);
            $segment = $request->segment(1);

            if (!in_array($segment, config('app.locales'))) {
                if (!session()->has('locale')) {
                    if (Cookie::has('locale') && in_array(Cookie::get('locale'), config('app.locales'))) {
                        session()->put('locale', Cookie::get('locale'));
                    } else {
                        $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
                        if (in_array($locale, config('app.locales'))) {
                            session()->put('locale', $locale);
                        } else {
                            session()->put('locale', config('app.fallback_locale'));
                        }
                    }
                }
                if (session()->has('locale')) {
                    App::setLocale(session()->get('locale'));
                }
                $segments = $request->segments();
                // var_dump($segments);
                $fallback = session('locale') ?: config('app.fallback_locale');
                $segments = \array_prepend($segments, $fallback);
                // $request->segments() = $segments;

                return redirect()->to(implode('/', $segments));
            }else{
                App::setLocale($segment);
            }

        }else{
            if (session()->has('locale')) {
                App::setLocale(session()->get('locale'));
            }
        }


    }
    public function handle($request, Closure $next)
    {
        $redirect = $this->changeLocale($request);
        // dd($redirect);
        if ($redirect != NULL) {
            return $redirect;
        }
        return $next($request);
    }
}
