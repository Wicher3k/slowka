<?php

namespace App\Http\Controllers;

use App\Category;
use App\Language;
use App\Flashcard;
use App\Word;
use Illuminate\Http\Request;
use Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::byId();
        return view('category.create')->with(compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = Language::byAcronym();
        $newCategory = new Category();
        $newCategory->name = $request->categoryName;
        $newCategory->user_id = Auth::user()->id;
        $newCategory->save();


        $langsFlashcards = array_keys($request->flashcard);
        foreach ($request->flashcard[$langsFlashcards[0]] as $index => $flashcard) {
            $flashcardNew = new Flashcard();
            $flashcardNew->user_id = Auth::user()->id;
            $flashcardNew->save();
            $newCategory->flashcards()->attach($flashcardNew);
            foreach ($langsFlashcards as $acronymLang) {
                if (!is_null($request->flashcard[$acronymLang][$index])) {
                    $word = new Word();
                    $word->language_id = $languages[$acronymLang]['id'];
                    $word->value = $request->flashcard[$acronymLang][$index];
                    $word->flashcard()->associate($flashcardNew);
                    $word->save();
                }
            }
        }
        return redirect()->route('myflashcards');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, Category $category)
    {
        $this->authorize('update', $category);
        return view('category.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
