<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;

class CookieController extends Controller
{
    private static $cookieMinutes = 525600;

    public static function setCookie($cookieArray)
    {
        [$nameCookie, $value] = $cookieArray;
        if ($nameCookie == 'userCookieConfirmed' && in_array($value, [true, false])) {
            Cookie::queue($nameCookie, $value, self::$cookieMinutes);
            Cookie::queue("cookieMinutesExperiationTimeFirstEntry", self::$cookieMinutes, self::$cookieMinutes);
            Cookie::queue("cookieMinutesDateFirstEntry", date("Y-m-d H:i:s"), self::$cookieMinutes);
        }elseif($nameCookie == 'langFrom' || $nameCookie=='langTo' ){
            Cookie::queue($nameCookie, $value, self::$cookieMinutes);
        }
    }

    public static function setCookiePost(Request $request)
    {
        self::setCookie([$request->cookieName, $request->value]);
        return response()->json('', 200);
    }
}
