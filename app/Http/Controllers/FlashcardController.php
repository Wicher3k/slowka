<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Category;
use App\Language;
use App\Flashcard;
use App\User;
use Illuminate\Support\Facades\Cookie;
use Auth;

class FlashcardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except('getData');
    }

    public function getData($idUser)
    {
        $languages = Language::byId();
        $languagesByAcronym = Language::byAcronym();
        $langFrom = $languagesByAcronym[Cookie::get('langFrom') ?? app()->getLocale()]->id;
        if (json_decode(Cookie::get('langTo')) != NULL) {
            $langTo = json_decode(Cookie::get('langTo'));
            foreach ($langTo as &$acronym) {
                $acronym = $languagesByAcronym[$acronym]->id;
            }
        } else {
            $langTo = [$languagesByAcronym[app()->getLocale()]->defaultTo];
        }

        $data = Category::with("flashcards", "flashcards.wordsRelation");
        if ($idUser != NULL) {
            $data = $data->where('user_id', $idUser);
        }else{
            $data = $data->where('user_id', 1);
        }
        $data = $data->get()->all();
        return [$languages, $langFrom, $langTo, $data];
    }

    public function index($lang = NULL, $user = NULL)
    {
        $user = $user ?? Auth::id() ;
        [$languages, $langFrom, $langTo, $data] = $this->getData($user);

        $trans = [];

        if (Auth::check() && Auth::id() == $user) {
            $trans['category'] = __('general.myCategories');
        } else {
            $trans['category'] = __('general.usersCategories',['login' => $user]);
        }
        return view('flashcard.index')->with(compact('data', 'languages', 'langFrom', 'langTo', 'trans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Flashcard  $flashcard
     * @return \Illuminate\Http\Response
     */
    public function show(Flashcard $flashcard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Flashcard  $flashcard
     * @return \Illuminate\Http\Response
     */
    public function edit(Flashcard $flashcard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Flashcard  $flashcard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flashcard $flashcard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Flashcard  $flashcard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flashcard $flashcard)
    {
        //
    }
}
