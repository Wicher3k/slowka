<?php
namespace App\Http\Controllers\ExcelGenerator;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class ExcelLoader {
    private $delimiter;
    private $filter;
    private $reader;
    private $readOnly;
    private $sheet;
    private $spreadsheet;
    private $sheetData = [];

    public function __construct($sheet, $readOnly, $delimiter = ";", $filter = ""){
        $this->sheet = $sheet;
        $this->delimiter = $delimiter;
        $this->filter = $filter;
        $this->readOnly = $readOnly;
        $this->loadFile();
    }

    private function loadFile() {
        if($this->identifyFileType() == 'Csv') {
            $this->reader = new Csv();
            $this->reader->setDelimiter($this->delimiter);
        }
        elseif($this->identifyFileType() == 'Xls')
            $this->reader = new Xls();
        elseif($this->identifyFileType() == 'Xlsx')
            $this->reader = new Xlsx();

        if($this->filter != "")
            $this->reader->setReadFilter($this->filter);

        $this->reader->setReadDataOnly($this->readOnly);
        $this->spreadsheet = $this->reader->load("tmp/" . $this->sheet['name']);
    }

    public function loadSheetToArray($number = 0) {
        $this->spreadsheet->setActiveSheetIndex($number);
        $this->sheetData = $this->spreadsheet->getActiveSheet()->toArray();

        return $this->sheetData;
    }

    public function identifyFileType() {
        return IOFactory::identify("tmp/" . $this->sheet['name']);
    }

    public function getSheet($number) {
        return $this->spreadsheet->getSheet($number);
    }

    public function getHighestDataRow() {
        return $this->spreadsheet->getActiveSheet()->getHighestDataRow();
    }

    public function getHighestColumn() {
        return $this->spreadsheet->getActiveSheet()->getHighestColumn();
    }

    public function getCellByColumnAndRow($col, $row) {
        return $this->spreadsheet->getActiveSheet()->getCellByColumnAndRow($col, $row);
    }

    public static function columnIndexFromString($string) {
        return Coordinate::columnIndexFromString($string);
    }

    public static function stringFromColumnIndex($index) {
        return Coordinate::stringFromColumnIndex($index);
    }

    public static function excelToDateTimeObject($date, $toString = false) {
        if(is_numeric($date) && $toString == false)
            return Date::excelToDateTimeObject($date);
        elseif(is_numeric($date) && $toString == true)
            return Date::excelToDateTimeObject($date)->format('Y-m-d');
        else
            return $date;
    }

}