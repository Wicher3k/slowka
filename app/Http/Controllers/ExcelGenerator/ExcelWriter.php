<?php
namespace App\Http\Controllers\ExcelGenerator;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelWriter
{
    private $spreadsheet;
    private $headers = [];
    private $rows = [];
    private $worksheetNames = [];
    private $configuration = [
        "mergeCells" => [],
        "cellsStyle" => [],
        "cellsType" => [],
        "columnsWidth" => [],
    ];

    private $insertedRows = 1;

    public function __construct($headers, $rows, $worksheetNames = [])
    {
        $this->setHeaders($headers);
        $this->addRows($rows);
        $this->setNamesWorksheets($worksheetNames);

        $this->spreadsheet = new Spreadsheet();
    }


    private function generateHeaders(&$sheet)
    {
        if ($this->headers) {
            if (is_array($this->headers[0])) { // header as matrix po przejsciu na php7 mozna użyć array_key_first
                foreach ($this->headers as $headerRow) {
                    $column = 0;
                    foreach ($headerRow as $value) {
                        $cell = $sheet->getCellByColumnAndRow(++$column, $this->insertedRows);
                        $cell->setValue($value);
                    }
                    ++$this->insertedRows;
                }
            } else { // header as row
                $column = 0;
                foreach ($this->headers as $value) {
                    $cell = $sheet->getCellByColumnAndRow(++$column, $this->insertedRows);
                    $cell->setValue($value);
                }
                ++$this->insertedRows;
            }
        }
    }

    private function generateRows(&$sheet, $format = null)
    {
        foreach ($this->rows as $rowData) {
            $column = 0;
            foreach ($rowData as $value) {
                if(isset($format) && $format == "text") {
                    $sheet->getCellByColumnAndRow(++$column, $this->insertedRows)->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                } else {
                    $sheet->setCellValueByColumnAndRow(++$column, $this->insertedRows, $value);
                }
            }
            ++$this->insertedRows;
        }
    }

    private function mergeCells(&$sheet)
    {
        foreach ($this->configuration['mergeCells'] as $cells) {
            $sheet->mergeCellsByColumnAndRow($cells[0][0], $cells[0][1], $cells[1][0], $cells[1][1]);
        }
    }

    private function getcellsStringRange($sheet, $cellsRange)
    {
        $cellStartColumn = $sheet->getCellByColumnAndRow($cellsRange[0][0], $cellsRange[0][1])->getColumn();
        $cellEndColumn = $sheet->getCellByColumnAndRow($cellsRange[1][0], $cellsRange[1][1])->getColumn();
        return $cellStartColumn . $cellsRange[0][1] . ":" . $cellEndColumn . $cellsRange[1][1];
    }

    private function applyCellsStyle(&$sheet)
    {
        foreach ($this->configuration['cellsStyle'] as $styleData) {
            foreach ($styleData[1] as $cellsRange) {
                $cellsStringRange = $this->getcellsStringRange($sheet, $cellsRange);
                $sheet->getStyle($cellsStringRange)->applyFromArray($styleData[0]);
            }
        }
        foreach ($this->configuration['cellsType'] as $styleData) {
            foreach ($styleData[1] as $cellsRange) {
                $cellsStringRange = $this->getcellsStringRange($sheet, $cellsRange);
                $sheet->getStyle($cellsStringRange)->getNumberFormat()->setFormatCode($styleData[0][0]);
            }
        }
        foreach ($this->configuration['columnsWidth'] as $styleData) {
            if (is_array($styleData[0])) {
                for ($i = $styleData[0][0]; $i <= $styleData[0][1]; ++$i) {
                    $cell = $sheet->getCellByColumnAndRow($i, 1);
                    $sheet->getColumnDimension($cell->getColumn())->setAutoSize(false)->setWidth($styleData[1]);
                }
            } else {
                $cell = $sheet->getCellByColumnAndRow($styleData[0], 1);
                $sheet->getColumnDimension($cell->getColumn())->setAutoSize(false)->setWidth($styleData[1]);
            }
        }
    }


    private function adjustWidths(&$sheet)
    {
        $numberOfColumns = count($this->headers);
        foreach ($this->rows as $row) {
            $count = count($row);
            if ($count > $numberOfColumns)
                $numberOfColumns = $count;
        }

        for ($i = 1; $i <= $numberOfColumns; $i++) {
            $cell = $sheet->getCellByColumnAndRow($i, 1);
            $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
        }
    }

    private function applyStyles(&$sheet)
    {
        $this->mergeCells($sheet);
        $this->adjustWidths($sheet);
        $this->applyCellsStyle($sheet);
    }

    private function setResponseHeaders($outputFileName,$extension)
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $outputFileName . '.'.$extension);
        header('Cache-Control: max-age=0');
    }

    private function generateWorksheetNames()
    {
        foreach ($this->worksheetNames as $i => $worksheetName) {
            $this->spreadsheet->setActiveSheetIndex($i)->setTitle($worksheetName);
        }
    }

    private function generateSheetFromCurrentData($format = null)
    {
        $sheet = $this->spreadsheet->getActiveSheet();

        $this->generateHeaders($sheet);
        $this->generateRows($sheet, $format);
        $this->applyStyles($sheet);
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    public function setNamesWorksheets($worksheetNames)
    {
        $this->worksheetNames = $worksheetNames;
    }

    public function addRows($rows)
    {
        $this->rows = array_merge($this->rows, $rows);
    }

    public function addConfiguration($options)
    {
        if (isset($options['mergeCells'])) {
            $this->configuration['mergeCells'] = array_merge($this->configuration['mergeCells'], $options['mergeCells']);
        }
        if (isset($options['cellsStyle'])) {
            $this->configuration['cellsStyle'] = array_merge($this->configuration['cellsStyle'], $options['cellsStyle']);
        }
        if (isset($options['cellsType'])) {
            $this->configuration['cellsType'] = array_merge($this->configuration['cellsType'], $options['cellsType']);
        }
        if (isset($options['columnsWidth'])) {
            $this->configuration['columnsWidth'] = array_merge($this->configuration['columnsWidth'], $options['columnsWidth']);
        }
    }

    public function getBinaryData()
    {
        $this->generateWorksheetNames();
        $this->generateSheetFromCurrentData();

        $writer = new Xlsx($this->spreadsheet);
        ob_start();
        $writer->save("php://output");
        $file = ob_get_clean();

        return $file;
    }

    public function getBinaryDataAjax()
    {
        $this->generateWorksheetNames();
        $this->generateSheetFromCurrentData();

        $writer = new Xlsx($this->spreadsheet);
        ob_start();
        $writer->save("php://output");
        $xlsData = ob_get_contents();
        ob_end_clean();
        $response =  array(
            'op' => 'ok',
            'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
        );
        die(json_encode($response));
    }

    public function generateExcel($outputFileName = "report", $outputLocation = "php://output", $exit = true, $format = null)
    {
        $this->generateWorksheetNames();
        $this->generateSheetFromCurrentData($format);
        $pathinfo = pathinfo($outputFileName);
        $outputFileName = $pathinfo['filename'];
        $ext = isset($pathinfo['extension'])?$pathinfo['extension'] : "";
        if($ext == ""){
            $ext = "xlsx";
        }
        if ($outputLocation == "php://output") {
            $this->setResponseHeaders($outputFileName,$ext);
            $outputFullPath = $outputLocation;
        } else {
            $outputFullPath = $outputLocation . "/" . $outputFileName . ".$ext";
        }
        if(strtolower($ext) == "csv"){
            $writer = new Csv($this->spreadsheet);
            $writer->setUseBOM(true);
            $writer->setDelimiter(';');
        }else{
            $writer = new Xlsx($this->spreadsheet);
        }

        $writer->save($outputFullPath);
        $this->spreadsheet->disconnectWorksheets();
        unset($spreadsheet);
        if ($exit) {
            exit;
        }
    }
}
