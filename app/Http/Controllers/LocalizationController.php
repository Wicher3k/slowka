<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App, Cookie;

class LocalizationController extends Controller
{
    public function lang($locale)
    {
        App::setLocale($locale);
        Cookie::queue('locale', $locale, 525600);
        session()->put('locale', $locale);
        $previousURL = url()->previous();
        $previousURLChange = str_replace(preg_filter('/^/', '/', config('app.locales')),"/".$locale,$previousURL);;
        return redirect()->to($previousURLChange);
    }
}
