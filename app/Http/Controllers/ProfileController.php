<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\{Category, Word, User};
use App\Http\Requests\UpdateUserProfile;
use App\Providers\RouteServiceProvider;
use Session;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $user = Auth::user();
        return view('profile.myProfile')->with(compact('user'));
    }

    public function changePassword(Request $request)
    {

        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required', 'min:8'],
            'new_confirm_password' => ['same:new_password', 'min:8'],
        ]);
        dd("asd");
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        dd('Password change successfully.');
    }

    public function update(UpdateUserProfile $request, $locale, User $user)
    {
        foreach (['newsletter'] as $box) {
            $request[$box] = isset($request['newsletter']) ? true : false;
        }
        $user->update($request->all());
        return redirect()->back()->withSuccess([__('general.notification.savedSuccessfully')]);
    }

    public function myflashcards()
    {
        return view('profile.index');
    }

    public function deactivation()
    {
        $user = Auth::user();
        return view('profile.deactivation')->with(compact('user'));
    }

    public function deactivationPost(Request $request)
    {
        return redirect('/'.Session::get('locale'))->withSuccess([ __('general.notification.deletedSuccessfully')]);
    }
}
