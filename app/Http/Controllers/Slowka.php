<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\ExcelGenerator\ExcelWriter;
use App\Http\Controllers\CookieController;
use App\Http\Controllers\FlashcardController;
use App\Category;
use App\Language;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Slowka extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    private function pairsLanguages()
    {
        $output = array();
        foreach ($this->languages as $langPrimary => $val) {
            foreach ($this->languages as $langSecondary => $cval) {
                if ($langPrimary == $langSecondary) {
                    continue;
                }
                $output[$langPrimary][] = $langSecondary;
            }
        }
        return $output;
    }

    public function index()
    {
        $FlashcardController = new FlashcardController();
        [$languages, $langFrom, $langTo, $data] = $FlashcardController->getData(NULL);
        return view('index')->with(compact('data', 'languages', 'langFrom', 'langTo'));
    }

    private function vocabularyGet(Request $request)
    {
        $from = $request->langFrom;
        $toArr = $request->langTo;
        $vocabulary = [];

        $data = Category::with("flashcards", "flashcards.wordsRelation", "flashcards.wordsRelation.wordInfos")->whereIn('name', $request->c)->get()->all();

        foreach ($data as $category) {
            foreach ($category->flashcards as $flashcard) {
                // foreach ($flashcard as $word) {
                $vocabulary[] = $flashcard->words;
            }
        }
        $languages = Language::byId();
        $toArrCookie = [];
        foreach (json_decode($toArr) as $idLang) {
            $toArrCookie[] = $languages[$idLang]->acronym;
        }
        CookieController::setCookie(['langFrom', $languages[$from]->acronym]);
        CookieController::setCookie(['langTo', json_encode($toArrCookie)]);
        return [$vocabulary, $from, $toArr, $data];
    }

    public function vocabulary(Request $request)
    {
        // dd($request->categories,$request->langFrom,$request->langTo);
        [$vocabulary, $from, $toArr, $data] = $this->vocabularyGet($request);
        return view('vocabulary')->with(compact('vocabulary', 'from', 'toArr'));
    }

    public function vocabularyListExcel($fromLang, $toArrLang, $flashcardsData, $type)
    {
        $headers = $dataExport = [];
        $languages = Language::byId();

        $headers[] = __('languages.' . $languages[$fromLang]->acronym);
        foreach ($toArrLang as $toLang) {
            $headers[] = __('languages.' . $languages[$toLang]->acronym);
        }

        $configuration['cellsStyle'] = [
            [
                [
                    'font' => [
                        'bold' => true,
                    ]
                ],
                [[[1, 1], [3, 1]]]
            ],

        ];
        foreach ($flashcardsData as $category) {

            foreach ($category->flashcards as $flashcard) {
                $row = [];
                $row[] = $flashcard->words[$fromLang]->value;

                foreach ($toArrLang as $langId) {
                    $row[] =  $flashcard->words[$langId]->value;
                }
                $dataExport[] = $row;
            }
        }
        $x = new ExcelWriter($headers, $dataExport);
        $x->addConfiguration($configuration);
        $x->generateExcel("myFlashcards." . strtolower($type));
        exit;
    }

    public function vocabularyList(Request $request)
    {
        [$vocabulary, $from, $toArr, $data] = $this->vocabularyGet($request);
        $toArr = json_decode($toArr);
        foreach ($data as $category) {
            $x = $category->flashcards->all();
            usort($x, function ($a, $b)  use ($from) {
                return $a->words[$from]->value > $b->words[$from]->value;
            });

            $category->flashcards = $x;
        }
        switch ($request->download) {
            case "Page":
                return view('vocabularyList')->with(compact('vocabulary', 'from', 'toArr', 'data'));
            case "CSV":
            case "XLSX":
                return $this->vocabularyListExcel($from, $toArr, $data, $request->download);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
