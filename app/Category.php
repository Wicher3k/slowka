<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Language;
use App\Flashcard;
use App\Word;
use App\WordInfo;

class Category extends Model
{
    public function flashcards()
    {
        return $this->belongsToMany('App\Flashcard')->withTimestamps();
    }

    public static function ImportXls(array $inputFileNameArr)
    {
        foreach ($inputFileNameArr as $inputFileName) {
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $spreadsheet = $reader->load($inputFileName);
            $data = $spreadsheet->getActiveSheet()->toArray();

            $languages = Language::pluck("id", "acronym")->all();
            $category = new Category();
            $category->user_id = "1";
            $langsCSV = [];
            foreach ($data as $index => $flashcard) {
                if ($index == 0) {
                    $category->name = $flashcard[0];
                    continue;
                } elseif ($index == 1) {
                    $category->save();
                    $langsCSV = $flashcard;
                    break;
                }
            }
            $temp = NULL;
            foreach ($data as $index => $flashcard) {
                if ($index == 0 || $index == 1) {
                    continue;
                } else {
                    $flashcard1 = new Flashcard();
                    $flashcard1->user_id = "1";
                    $flashcard1->save();
                    $saveFlashcard = false;
                    foreach ($langsCSV as $indexLang => $langCSV) {
                        if ($flashcard[$indexLang] != '') {
                            if (isset($languages[$langCSV])) {
                                $value = $flashcard[$indexLang];
                                if (isset($value) && $value != '') {
                                    if (strpos($value, 'HSK')) {
                                        continue 2;
                                    }
                                    // wylaczenie HSK

                                    $word = new Word();
                                    $word->language_id = $languages[$langCSV];

                                    if ($langCSV != "ch_2") {
                                        $value = trim(ucfirst($value));
                                    }
                                    $word->value = $value;
                                    // $flashcard1->wordsRelation()->save($word);
                                    $word->flashcard()->associate($flashcard1);
                                    $word->save();
                                    $temp[$languages[$langCSV]] = $word;
                                    $saveFlashcard = true;
                                }
                            } elseif ($langCSV == 'komentarz_pl') {
                                $wordInfo = new WordInfo();
                                $wordInfo->word_id = $temp[2]->id;
                                $wordInfo->hint = $flashcard[$indexLang];
                                $wordInfo->save();
                            }
                        }
                    }
                    if ($saveFlashcard) {

                        $category->flashcards()->attach($flashcard1);
                    }
                }
            }
        }
    }
}
