<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertKoreanBasics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $inputFileNameArr = ["storage/app/flashcards/basicKorean.xlsx"];
        // Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["Korean Basics"])->delete();
    }
}
