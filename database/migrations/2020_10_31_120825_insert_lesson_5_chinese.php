<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;



class InsertLesson5Chinese extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/Chinese 5a words.xlsx", "storage/app/flashcards/Chinese 5b words.xlsx", "storage/app/flashcards/Chinese 5a sentences.xlsx","storage/app/flashcards/Chinese 5b sentences.xlsx"];
        // Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ['Chinese - Lesson 5a - Words', 'Chinese - Lesson 5b - Words', 'Chinese - Lesson 5a - Sentences', 'Chinese - Lesson 5b - Sentences'])->delete();
    }
}
