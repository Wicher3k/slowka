<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Language;
use App\Category;
use App\Flashcard;
use App\Word;

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class InsertNewEntries2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */



    public function up()
    {
        $inputFileName = "storage/app/flashcards/50 Popular Verbs.xlsx";

        /**  Identify the type of $inputFileName  **/
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();

        $languages = Language::pluck("id", "acronym")->all();
        $category = new Category();
        $category->user_id = "1";
        $langsCSV = [];
        foreach ($data as $index => $flashcard) {
            if ($index == 0) {
                $category->name = $flashcard[0];
                continue;
            } elseif ($index == 1) {
                $category->save();
                $langsCSV = $flashcard;
                break;
            }
        }

        foreach ($data as $index => $flashcard) {
            if ($index == 0 || $index == 1) {
                continue;
            } else {
                $flashcard1 = new Flashcard();
                $flashcard1->user_id = "1";
                $flashcard1->save();
                $category->flashcards()->attach($flashcard1);
                foreach ($langsCSV as $indexLang => $langCSV) {
                    $word = new Word();
                    $word->language_id = $languages[$langCSV];
                    $value = $flashcard[$indexLang];
                    if($langCSV != "ch_2"){
                        $value = ucfirst($value);
                    }
                    $word->value = $value;
                    // $flashcard1->wordsRelation()->save($word);
                    $word->flashcard()->associate($flashcard1);
                    $word->save();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
