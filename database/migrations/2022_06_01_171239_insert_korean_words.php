<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertKoreanWords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/slowka1.xlsx", "storage/app/flashcards/slowka2.xlsx", "storage/app/flashcards/slowka3.xlsx", "storage/app/flashcards/slowka4.xlsx", "storage/app/flashcards/podstawoweZwroty.xlsx", "storage/app/flashcards/przedmiotySzkola.xlsx", "storage/app/flashcards/gdziejest.xlsx", "storage/app/flashcards/prosze.xlsx", "storage/app/flashcards/koslowka2.xlsx","storage/app/flashcards/czynnosci.xlsx"];
        Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["Korean basic words cz1", "Korean basic words cz2", "Korean basic words cz3", "Korean basic words cz4","Podstawowe zwroty","Przedmioty - Szkoła i codziennego użytku", "Proszę, i", "Gdzie? Znajduje się, Być","Słówka cz2","Czynności"])->delete();
    }
}
