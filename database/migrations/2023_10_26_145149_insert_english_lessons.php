<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = [
            "storage/app/flashcards/angielski/angielski Adac 2 brain.xlsx",
            "storage/app/flashcards/angielski/angielski Adac 2.xlsx",
            "storage/app/flashcards/angielski/angielski book 1 sentences.xlsx",
            "storage/app/flashcards/angielski/angielski book 1.xlsx",
            "storage/app/flashcards/angielski/angielski book 2.xlsx",
        ];

        Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', [

            'Angielski Adac 2',
            'Angielski Adac 2 brainstorming',
            'Angielski book 2',
            'Angielski book 1',
            'Angielski book 1 Sentences',

        ])->delete();
    }
};
