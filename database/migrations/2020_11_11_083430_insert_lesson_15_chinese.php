<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertLesson15Chinese extends Migration
{
    private $lesson = "15";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/Chinese {$this->lesson}a words.xlsx", "storage/app/flashcards/Chinese {$this->lesson}b words.xlsx", "storage/app/flashcards/Chinese {$this->lesson}a sentences.xlsx","storage/app/flashcards/Chinese {$this->lesson}b sentences.xlsx"];
        // Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["Chinese - Lesson {$this->lesson}a - Words", "Chinese - Lesson {$this->lesson}b - Words","Chinese - Lesson {$this->lesson}a - Sentences", "Chinese - Lesson {$this->lesson}b - Sentences"])->delete();
    }
}
