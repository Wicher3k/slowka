<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Language;
use App\Category;
use App\Flashcard;
use App\Word;

class InsertNewEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            "colors" => [
                ['pl' => "Biały", "en" => 'White', "ch" => "Bái", "ch_2" => "白"],
                ['pl' => "Czarny", "en" => 'Black', "ch" => "Hēi", "ch_2" => "黑"],
                ['pl' => "Szary", "en" => 'Gray', "ch" => "Huī", "ch_2" => "灰"],
                ['pl' => "Żółty", "en" => 'Yellow',  "ch" => "Huáng", "ch_2" => "黄"],
                ['pl' => "Zielony", "en" => 'Green',  "ch" => "Lǜ", "ch_2" => "绿"],
                ['pl' => "Czerwony", "en" => 'Red',  "ch" => "Hóng", "ch_2" => "红"],
                ['pl' => "Niebieski", "en" => 'Blue',  "ch" => "Lán", "ch_2" => "蓝"],
                ['pl' => "Brązowy", "en" => 'Brown',  "ch" => "Hé", "ch_2" => "褐"],
                ['pl' => "Pomarańczowy", "en" => 'Orange',  "ch" => "Chéng", "ch_2" => "橙"],
                ['pl' => "Fioletowy", "en" => 'Violet', "ch" => "Zǐ", "ch_2" => "紫"],
                ['pl' => "Różowy", "en" => 'Pink',  "ch" => "Fěnhóng", "ch_2" => "粉红"],
                ['pl' => "Srebny", "en" => 'Silver',  "ch" => "Yín", "ch_2" => "银"],
                ['pl' => "Złoty", "en" => 'Gold',  "ch" => "Jīn", "ch_2" => "金"],
            ],
            "numbers" => [
                ['pl' => "Zero", "en" => "Zero", "ch" => "Líng", "ch_2" => "零"],
                ['pl' => "Jeden", "en" => "One",  "ch" => "Yī", "ch_2" => "一"],
                ['pl' => "Dwa", "en" => "Two", "ch" => "Èr", "ch_2" => "二"],
                ['pl' => "Trzy", "en" => "Three", "ch" => "Sān", "ch_2" => "三"],
                ['pl' => "Cztery", "en" => "Four", "ch" => "Sì", "ch_2" => "四"],
                ['pl' => "Pięć", "en" => "Five", "ch" => "Wǔ", "ch_2" => "五"],
                ['pl' => "Sześć", "en" => "Six", "ch" => "Liù", "ch_2" => "六"],
                ['pl' => "Siedem", "en" => "Seven", "ch" => "Qī", "ch_2" => "七"],
                ['pl' => "Osiem", "en" => "Eight", "ch" => "Bā", "ch_2" => "八"],
                ['pl' => "Dziewięć", "en" => "Nine", "ch" => "Jiǔ", "ch_2" => "九"],
                ['pl' => "Dziesięć", "en" => "Ten", "ch" => "Shí", "ch_2" => "十"],
                ['pl' => "Sto", "en" => "Hundred", "ch" => "Bǎi", "ch_2" => "百"],
                ['pl' => "Tysiąć", "en" => "Thousand", "ch" => "Qiān", "ch_2" => "千"],
                ['pl' => "Dziesięć tysięcy", "en" => "Ten thousand", "ch" => "Wàn", "ch_2" => "万"],
            ],
            "dateAndTime" => [
                ['pl' => "Godzina", "en" => "Hour", "ch" => "Diǎn", "ch_2" => "点"],
                ['pl' => "Miesiąc", "en" => "Month",  "ch" => "Yuè", "ch_2" => "月"],
                ['pl' => "Rok", "en" => "Year", "ch" => "Nián", "ch_2" => "年"],
                ['pl' => "Minuta", "en" => "Minute", "ch" => "Fēn", "ch_2" => "分"],
                ['pl' => "Kwadrans", "en" => "Quarter of an hour", "ch" => "Kè", "ch_2" => "刻"],
                ['pl' => "Pół", "en" => "Half", "ch" => "Bàn", "ch_2" => "半"],
                ['pl' => "8 Rano", "en" => "8 AM", "ch" => "Zǎoshang bā diǎn", "ch_2" => "早上 八 点"],
                ['pl' => "8 Wieczór", "en" => "8 PM", "ch" => "Wǎnshàng bā diǎn", "ch_2" => "晚上 八 点"],
                ['pl' => "Poniedziałek", "en" => "Monday", "ch" => "Xīngqíyī", "ch_2" => "星期一"],
                ['pl' => "Wtorek", "en" => "Tuesday", "ch" => "Xīngqíèr", "ch_2" => "星期二"],
                ['pl' => "Środa", "en" => "Wednesday", "ch" => "Xīngqísān", "ch_2" => "星期三"],
                ['pl' => "Czwartek", "en" => "Thursday", "ch" => "Xīngqísì", "ch_2" => "星期四"],
                ['pl' => "Piątek", "en" => "Friday", "ch" => "Xīngqíwǔ", "ch_2" => "星期五"],
                ['pl' => "Sobota", "en" => "Saturday", "ch" => "Xīngqíliù", "ch_2" => "星期六"],
                ['pl' => "Niedziela", "en" => "Sunday", "ch" => "Xīngqírì / Xīngqítiān", "ch_2" => "星期日 / 星期天"],
            ],
            "fruits" => [
                ['pl' => "Owoce", "en" => "Fruits", "ch" => "Shuǐguǒ", "ch_2" => "水果"],
                ['pl' => "Truskawka", "en" => "Strawberry",  "ch" => "Cǎoméi", "ch_2" => "草莓"],
                ['pl' => "Wiśnia", "en" => "Cherry", "ch" => "Yīngtáo", "ch_2" => "樱桃"],
                ['pl' => "Cytryna", "en" => "Lemon", "ch" => "Níngméng", "ch_2" => "柠檬"],
                ['pl' => "Pomarańcza", "en" => "Orange", "ch" => "Júzi", "ch_2" => "橘子"],
                ['pl' => "Grejpfrut", "en" => "Grapefruit", "ch" => "Pútáo yòu", "ch_2" => "葡萄柚"],
                ['pl' => "Brzoskwinia", "en" => "Peach", "ch" => "Táozi", "ch_2" => "桃子"],
                ['pl' => "Ananas", "en" => "Pineapple", "ch" => "Bōluó", "ch_2" => "菠萝"],
                ['pl' => "Arbuz", "en" => "Watermelon", "ch" => "Xīguā", "ch_2" => "西瓜"],
                ['pl' => "Banan", "en" => "Banana", "ch" => "Xiāngjiāo", "ch_2" => "香蕉"],
                ['pl' => "Winogrono", "en" => "Grape", "ch" => "Pútáo", "ch_2" => "葡萄"],
                ['pl' => "Jabłko", "en" => "Apple", "ch" => "Píngguǒ", "ch_2" => "苹果"],
                ['pl' => "Gruszka", "en" => "Pear", "ch" => "Lí", "ch_2" => "梨"],
                ['pl' => "Śliwka", "en" => "Plum", "ch" => "Lǐzǐ", "ch_2" => "李子"],
                ['pl' => "Morela", "en" => "Apricot", "ch" => "Xìng", "ch_2" => "杏"],
                ['pl' => "Kiwi", "en" => "Kiwi", "ch" => "Míhóutáo", "ch_2" => "猕猴桃"],
            ],
            "vegetables" => [
                ['pl' => "Warzywa", "en" => "Vegetables", "ch" => "Shūcài", "ch_2" => "蔬菜"],
                ['pl' => "Cebula", "en" => "Onion",  "ch" => "Yángcōng", "ch_2" => "洋葱"],
                ['pl' => "Ogórek", "en" => "Cucumber", "ch" => "Huángguā", "ch_2" => "黄瓜"],
                ['pl' => "Ziemniak", "en" => "Potato", "ch" => "Tǔdòu", "ch_2" => "土豆"],
                ['pl' => "Pomidor", "en" => "Tomato", "ch" => "Xīhóngshì", "ch_2" => "西红柿"],
                ['pl' => "Marchew", "en" => "Carrot", "ch" => "Húluóbo", "ch_2" => "胡萝卜"],
                ['pl' => "Kukurydza", "en" => "Corn", "ch" => "Yùmǐ", "ch_2" => "玉米"],
                ['pl' => "Sałata", "en" => "Lettuce", "ch" => "Shēngcài", "ch_2" => "生菜"],
                ['pl' => "Kapusta", "en" => "Cabbage", "ch" => "Báicài", "ch_2" => "白菜"],
                ['pl' => "Seler", "en" => "Celery", "ch" => "Qíncài", "ch_2" => "芹菜"],
                ['pl' => "Fasola", "en" => "Bean", "ch" => "Dòu", "ch_2" => "豆"],
                ['pl' => "Rzodkiewka", "en" => "Radish", "ch" => "Luóbo", "ch_2" => "萝卜"],
                ['pl' => "Szpinak", "en" => "Spinach", "ch" => "Bōcài", "ch_2" => "菠菜"],
            ],
            "dairy" => [
                ['pl' => "Nabiał", "en" => "Dairy", "ch" => "Rǔ zhìpǐn", "ch_2" => "乳制品"],
                ['pl' => "Mleko", "en" => "Milk",  "ch" => "Niúnǎi", "ch_2" => "牛奶"],
                ['pl' => "Lody", "en" => "Ice cream", "ch" => "Bīngqílín", "ch_2" => "冰淇淋"],
                ['pl' => "Masło", "en" => "Butter", "ch" => "Huángyóu", "ch_2" => "牛油"],
                ['pl' => "Śmietana", "en" => "Sour cream", "ch" => "Suānnǎi yóu", "ch_2" => "酸奶 油"],
                ['pl' => "Jogurt", "en" => "Yogurt", "ch" => "Suānnǎi", "ch_2" => "酸奶"],
            ],
            "other" => [
                ['pl' => "Jajko", "en" => "Egg", "ch" => "Jīdàn", "ch_2" => "鸡蛋"],
                ['pl' => "Mięso", "en" => "Meat",  "ch" => "Ròu", "ch_2" => "肉"],
                ['pl' => "Ryba", "en" => "Fish", "ch" => "Yú", "ch_2" => "鱼"],
                ['pl' => "Ryż", "en" => "Rice", "ch" => "Mǐfàn", "ch_2" => "米饭"],
                ['pl' => "Makaron", "en" => "Pasta", "ch" => "Miànshí", "ch_2" => "面食"],
                ['pl' => "Pierogi", "en" => "Dumplings", "ch" => "Jiǎozi", "ch_2" => "饺子"],
                ['pl' => "Frytki", "en" => "Chips", "ch" => "Shǔ tiáo", "ch_2" => "薯 条"],
                ['pl' => "Cukier", "en" => "Sugar", "ch" => "Táng", "ch_2" => "糖"],
                ['pl' => "Czekolada", "en" => "Chocolate", "ch" => "Qiǎokèlì", "ch_2" => "巧克力"],
                ['pl' => "Mąka", "en" => "Flour", "ch" => "Miànfěn", "ch_2" => "面粉"],
            ],
            "drinks" => [
                ['pl' => "Kawa", "en" => "Coffee", "ch" => "Kāfēi", "ch_2" => "咖啡"],
                ['pl' => "Herbata", "en" => "Tea",  "ch" => "Chá", "ch_2" => "茶"],
                ['pl' => "Woda gazowana", "en" => "Sparkling water", "ch" => "Qìshuǐ", "ch_2" => "汽水"],
                ['pl' => "Woda", "en" => "Water", "ch" => "Shuǐ", "ch_2" => "水"],
                ['pl' => "Sok pomarańczowy", "en" => "Orange juice", "ch" => "Júzi shuǐ", "ch_2" => "桔子 水"],
                ['pl' => "Lemoniada", "en" => "lemonade", "ch" => "Níngméng shuǐ", "ch_2" => "柠檬 水"],
            ],
            "extras" => [
                ['pl' => "Musztarda", "en" => "Mustard",  "ch" => "Jièmò jiàng", "ch_2" => "芥末 酱"],
                ['pl' => "Keczup", "en" => "Ketchup", "ch" => "Fānqié jiàng", "ch_2" => "番茄酱"],
                ['pl' => "Majonez", "en" => "Mayonnaise", "ch" => "Dànhuáng jiàng", "ch_2" => "蛋黄酱"],
                ['pl' => "Oliwa", "en" => "Oil", "ch" => "Yóu", "ch_2" => "油"],
                ['pl' => "Ocet", "en" => "Vinegar", "ch" => "Cù", "ch_2" => "醋"],
                ['pl' => "Miód", "en" => "Honey", "ch" => "Fēngmì", "ch_2" => "蜂蜜"],
                ['pl' => "Dżem", "en" => "Jam", "ch" => "Guǒjiàng", "ch_2" => "果酱"],
            ],
        ];
        $languages = Language::pluck("id","acronym")->all();

        foreach($data as $categoryName => $flashcards){
            $category = new Category();
            $category->user_id = "1";
            $category->name = $categoryName;
            $category->save();
            foreach($flashcards as $flashcard){
                $flashcard1 = new Flashcard();
                $flashcard1->user_id = "1";
                $flashcard1->save();
                $category->flashcards()->attach($flashcard1);
                foreach($flashcard as $acronymLang => $value){
                    $word = new Word();
                    $word->language_id =$languages[$acronymLang];
                    $word->value =$value;
                    // $flashcard1->wordsRelation()->save($word);
                    $word->flashcard()->associate($flashcard1);
                    $word->save();
                }
            }



        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
