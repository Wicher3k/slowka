<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class InsertUserSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = new User();
        $user->email = "roland.gornisiewicz@gmail.com";
        $user->password = Hash::make("1234qwer");
        $user->newsletter = true;
        $user->policy = true;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = User::where('email', "roland.gornisiewicz@gmail.com")->first();
        $user->delete();
    }
}
