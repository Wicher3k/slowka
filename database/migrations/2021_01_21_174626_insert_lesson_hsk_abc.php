<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertLessonHskAbc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $inputFileNameArr = ["storage/app/flashcards/HSK 3 ABC cz1-5.xlsx", "storage/app/flashcards/HSK 3 ABC cz6-8.xlsx", "storage/app/flashcards/HSK 3 ABC cz9-10.xlsx", "storage/app/flashcards/HSK 3 ABC cz11-13.xlsx", "storage/app/flashcards/HSK 3 ABC cz14-16.xlsx"];
        Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["HSK3 ABC - cz1", "HSK3 ABC - cz2", "HSK3 ABC - cz3", "HSK3 ABC - cz4", "HSK3 ABC - cz5"])->delete();
    }
}
