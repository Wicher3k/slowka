<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertLessonBChinese extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/Chinese B1.xlsx", "storage/app/flashcards/Chinese B2.xlsx", "storage/app/flashcards/Chinese B3.xlsx", "storage/app/flashcards/Chinese B4.xlsx", "storage/app/flashcards/Chinese B5.xlsx", "storage/app/flashcards/Chinese C1.xlsx", "storage/app/flashcards/Chinese C2.xlsx", "storage/app/flashcards/Chinese C3.xlsx"];
        Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["Chinese B1 - Words", "Chinese B2 - Words", "Chinese B3 - Words", "Chinese B4 - Words", "Chinese B5 - Words", "Chinese C1 - Words", "Chinese C2 - Words",  "Chinese C3 - Words"])->delete();
    }
}
