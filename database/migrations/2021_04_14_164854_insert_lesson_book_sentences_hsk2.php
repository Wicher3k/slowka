<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertLessonBookSentencesHsk2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/HSK 2 Book 1.xlsx", "storage/app/flashcards/HSK 2 Book 2.xlsx", "storage/app/flashcards/HSK 2 Book 3.xlsx", "storage/app/flashcards/HSK 2 Book 4.xlsx","storage/app/flashcards/HSK 2 Book 5.xlsx","storage/app/flashcards/HSK 2 Book 6.xlsx","storage/app/flashcards/HSK 2 Book 7.xlsx","storage/app/flashcards/HSK 2 Book 8.xlsx","storage/app/flashcards/HSK 2 Book 9.xlsx","storage/app/flashcards/HSK 2 Book 10.xlsx","storage/app/flashcards/HSK 2 Book 11.xlsx","storage/app/flashcards/HSK 2 Book 12.xlsx","storage/app/flashcards/HSK 2 Book 13.xlsx","storage/app/flashcards/HSK 2 Book 14.xlsx","storage/app/flashcards/HSK 2 Book 15.xlsx"];
        // Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["HSK 2 Book 1", "HSK 2 Book 2", "HSK 2 Book 3", "HSK 2 Book 4", "HSK 2 Book 5", "HSK 2 Book 6", "HSK 2 Book 7", "HSK 2 Book 8", "HSK 2 Book 9", "HSK 2 Book 10", "HSK 2 Book 11", "HSK 2 Book 12", "HSK 2 Book 13", "HSK 2 Book 14", "HSK 2 Book 15"])->delete();
    }
}
