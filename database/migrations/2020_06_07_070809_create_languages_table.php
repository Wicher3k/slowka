<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Language;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->id();
            $table->string('acronym', 10)->unique();
            $table->tinyInteger('defaultTo');

        });

        $language = new Language();
        $language->acronym = "en";
        $language->defaultTo = 3;
        $language->save();

        $language = new Language();
        $language->acronym = "pl";
        $language->defaultTo = 3;
        $language->save();

        $language = new Language();
        $language->acronym = "ch";
        $language->defaultTo = 1;
        $language->save();

        $language = new Language();
        $language->acronym = "ch_2";
        $language->defaultTo = 1;
        $language->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
