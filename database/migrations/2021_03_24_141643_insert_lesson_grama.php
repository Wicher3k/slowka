<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertLessonGrama extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/zdania_twierdzace.xlsx", "storage/app/flashcards/zdania_przeczace.xlsx", "storage/app/flashcards/zdania_pytajace.xlsx", "storage/app/flashcards/zdania_rozkazujace.xlsx", "storage/app/flashcards/czasowniki_modalne.xlsx", "storage/app/flashcards/liczebniki.xlsx", "storage/app/flashcards/klasyfikatory_rzeczownikowe.xlsx", "storage/app/flashcards/klasyfikatory_czasownikowe.xlsx", "storage/app/flashcards/klasyfikatory_czasownikowe_zdania.xlsx", "storage/app/flashcards/Reduplikacja.xlsx", "storage/app/flashcards/wyrazanie opinii.xlsx"];
        Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["Zdania pytajace", "Zdania rozkazujące", "Zdania twiedzące", "Zdania przeczące", "Czasowniki modalne", "Liczebniki", "Klasyfikatory rzeczownikowe", "Klasyfikatory czasownikowe", "Klasyfikatory czasownikowe zdania", "Reduplikacja", "Wyrażanie opinii"])->delete();
    }
}
