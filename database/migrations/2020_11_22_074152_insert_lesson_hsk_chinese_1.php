<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Category;

class InsertLessonHskChinese1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/HSK 1.xlsx", "storage/app/flashcards/HSK 2.xlsx", "storage/app/flashcards/HSK 3.xlsx", "storage/app/flashcards/HSK 3 Struktury i zdania.xlsx", "storage/app/flashcards/HSK 3+.xlsx"];
        Category::ImportXls($inputFileNameArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ["HSK 1 Chinese - Words", "HSK 1 Chinese - Sentences","HSK 2 Chinese - Words", "HSK 2 Chinese - Sentences","HSK 3 Chinese - Words", "HSK 3 Chinese - Sentences","HSK 3+ Chinese - Words"])->delete();
    }
}
