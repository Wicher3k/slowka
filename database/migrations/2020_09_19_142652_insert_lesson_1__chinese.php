<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Language;
use App\Category;
use App\Flashcard;
use App\Word;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class InsertLesson1Chinese extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $inputFileNameArr = ["storage/app/flashcards/Chinese 1a words.xlsx","storage/app/flashcards/Chinese 1a sentences.xlsx","storage/app/flashcards/Chinese 1b words.xlsx","storage/app/flashcards/Chinese 1b sentences.xlsx"];
        // foreach ($inputFileNameArr as $inputFileName) {
        //     $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        //     $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        //     $spreadsheet = $reader->load($inputFileName);
        //     $data = $spreadsheet->getActiveSheet()->toArray();

        //     $languages = Language::pluck("id", "acronym")->all();
        //     $category = new Category();
        //     $category->user_id = "1";
        //     $langsCSV = [];
        //     foreach ($data as $index => $flashcard) {
        //         if ($index == 0) {
        //             $category->name = $flashcard[0];
        //             continue;
        //         } elseif ($index == 1) {
        //             $category->save();
        //             $langsCSV = $flashcard;
        //             break;
        //         }
        //     }

        //     foreach ($data as $index => $flashcard) {
        //         if ($index == 0 || $index == 1) {
        //             continue;
        //         } else {
        //             $flashcard1 = new Flashcard();
        //             $flashcard1->user_id = "1";
        //             $flashcard1->save();
        //             $category->flashcards()->attach($flashcard1);
        //             foreach ($langsCSV as $indexLang => $langCSV) {
        //                 $word = new Word();
        //                 $word->language_id = $languages[$langCSV];
        //                 $value = $flashcard[$indexLang];
        //                 if ($langCSV != "ch_2") {
        //                     $value = trim(ucfirst($value));
        //                 }
        //                 $word->value = $value;
        //                 // $flashcard1->wordsRelation()->save($word);
        //                 $word->flashcard()->associate($flashcard1);
        //                 $word->save();
        //             }
        //         }
        //     }
        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::whereIn('name', ['Chinese - Lesson 1a - Words', 'Chinese - Lesson 1a - Sentences', 'Chinese - Lesson 1b - Words', 'Chinese - Lesson 1b - Sentences'])->delete();
    }
}
