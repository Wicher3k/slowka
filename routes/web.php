<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (!function_exists('myRoutes')) {
    function myRoutes()
    {
        $group = Route::group([], function () {

            Route::get('', 'Slowka@index')->name('categories.index');
            Route::group(['prefix' => 'categories'], function () {
                Route::get('', 'Slowka@index')->name('categories.index');
                Route::get('vocabulary', 'Slowka@vocabulary')->name('categories.vocabulary');
                Route::get('vocabularyList', 'Slowka@vocabularyList')->name('categories.vocabularyList');
                Route::get('edit/{category}', 'CategoryController@edit')->name('categories.edit');
                Route::get('create', "CategoryController@create")->name('categories.create');
                Route::post('store', "CategoryController@store")->name('categories.store');
                // // Route::put('/update', "CategoryController@update")->name('categories.update');
            });
            Route::group(['prefix' => 'profile'], function () {
                Route::get('/', 'ProfileController@index')->name('profile.index');
                Route::post('/', 'ProfileController@changePassword')->name('profile.changePassword');
                Route::put('/{user}', 'ProfileController@update')->name('profile.update');
                // Route::get('/stats', 'ProfileController@index')->name('profile.index');
                Route::get('/myflashcards', 'ProfileController@myflashcards')->name('profile.myflashcards');
                Route::get('/deactivation', 'ProfileController@deactivation')->name('profile.deactivation');
                Route::post('/deactivation', 'ProfileController@deactivationPost')->name('profile.deactivationPost');
            });

            Route::get('cookiePolicy', "Contact@index")->name('contact.cookiePolicy');
            Route::get('contact', "Contact@index")->name('contact.contact');


            Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
            Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');


            Route::get('home', 'HomeController@index')->name('home');

            Route::group(['prefix' => 'myflashcards', 'middleware' => ['verified']], function () {
                Route::get('{user?}', "FlashcardController@index")->name('myflashcards');
            });
        });
        return $group;
    }
}


Route::post('/cookieSet', 'CookieController@setCookiePost');

Route::group(['prefix' => '{locale?}', 'middleware' => ['localization']], function () {
    myRoutes();
});

// Route::group(['middleware' => ['localization']], function () { // jak to odkomentuje to sie zepsuje.
//     myRoutes();
// });



// Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::group(['middleware' => 'localizationWithoutChangingURL'], function () {
    Auth::routes(['verify' => true]);
});
Route::get('lang', 'LocalizationController@lang')->name('lang');
Route::group(['prefix' => '{locale?}'], function () {
    Route::get('lang', 'LocalizationController@lang')->name('lang');
});
